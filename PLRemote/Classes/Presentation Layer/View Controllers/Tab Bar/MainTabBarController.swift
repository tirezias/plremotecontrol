//
//  MainTabBarController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/27/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreBluetooth

class MainTabBarController: BaseTabBarController {

    let serviceDevice = ServiceDevice()
    var previousSelectedTabIndex:Int = 0
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupNotifications()
        setupController()
    }

    override func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        previousSelectedTabIndex = tabBarController.selectedIndex
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        let vc = self.viewControllers![previousSelectedTabIndex] as! UINavigationController
        vc.popToRootViewController(animated: false)
    }
    
    
    // MARK: - Private methods
    private func setupController() {
        
        if(BluetoothManager.shared.vmDevice == nil) {
            
            if let vmDevice = serviceDevice.getLastConnectedDevice() {
             
                BluetoothManager.shared.vmDevice = vmDevice
            }
        }
        BluetoothManager.shared.setupBLE()
        BluetoothManager.shared.startScan()
    }
    
    private func setupNotifications() {
    
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.newDeviceFound(notification:)),
                                               name: NSNotification.Name(rawValue: DEVICE_FOUND),
                                               object: nil)
    }
    
    private func checkIsExistPeripheralDevice(_ identifier:String) -> Bool {
        
        if let _ = serviceDevice.getDeviceWithIdentifier(identifier) {
            
            return true
        }
        return false
    }
    
    private func checkIsExistConnectedDevice() -> Bool {
        
        if(BluetoothManager.shared.vmDevice != nil) {
            
            return BluetoothManager.shared.vmDevice!.isConnected
        }
        
        return false
    }
    
    @objc private func newDeviceFound(notification:Notification) {
        
        let dict = notification.userInfo
        let viewModel:VMDevice = dict![DEVICE_FOUND] as! VMDevice
        
        if (checkIsExistPeripheralDevice(viewModel.identifier) && !checkIsExistConnectedDevice()) {
            
            BluetoothManager.shared.vmDevice = viewModel
            BluetoothManager.shared.connect(viewModel)
        }
    }
}
