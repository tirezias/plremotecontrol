//
//  HelpViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/31/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

class HelpViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var buttonLetsGo: UIButton!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var pageControllView: UIView!
    @IBOutlet weak var constraitWidthPageControl:NSLayoutConstraint!
    
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        scrollView.contentSize = CGSize(width: 3 * UIScreen.main.bounds.width, height: 0)
        scrollView.delegate = self
    }
    
    
    // MARK: - Private methods
    private func setupButtonsWithPage(_ page:Int) {
        
        switch page {
            case 0:
                self.constraitWidthPageControl.constant = 6
                UIView.animate(withDuration: 0.2, animations: {
                                
                    self.buttonLetsGo.alpha = 1
                    self.buttonNext.alpha = 0
                    self.buttonBack.alpha = 0
                    self.view.layoutIfNeeded()
                }, completion: nil)
            case 1:
                self.constraitWidthPageControl.constant = self.pageControllView.frame.size.width/2 + 3
                UIView.animate(withDuration: 0.2, animations: {
                                
                    self.buttonLetsGo.alpha = 0
                    self.buttonNext.alpha = 1
                    self.buttonBack.alpha = 1
                    self.view.layoutIfNeeded()
                }, completion: nil)
            case 2:
                self.constraitWidthPageControl.constant = self.pageControllView.frame.size.width
                UIView.animate(withDuration: 0.2, animations: {
                                
                    self.buttonLetsGo.alpha = 0
                    self.buttonNext.alpha = 1
                    self.buttonBack.alpha = 1
                    self.view.layoutIfNeeded()
                }, completion: nil)
        default:
            break
        }
    }
    
    
    // MARK: - Action methods
    @IBAction func onClickLetsGo(_ sender: Any) {
        
        scrollView.setContentOffset(CGPoint(x: view.frame.size.width, y: scrollView.contentOffset.y), animated: true)
        setupButtonsWithPage(1)
    }
    
    @IBAction func onClickNext(_ sender: Any) {
     
        switch scrollView.contentOffset.x {
        case view.frame.width:
            scrollView.setContentOffset(CGPoint(x: 2*view.frame.width, y: scrollView.contentOffset.y), animated: true)
            setupButtonsWithPage(2)
        case 2*view.frame.width:
            pushControllerWithStoryboard("NewDevice", storyboardId: "SelectPLRemoteViewController")
        default:
            break
        }
    }
    
    @IBAction func onClickBack(_ sender: Any) {
     
        switch scrollView.contentOffset.x {
        case view.frame.width:
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y), animated: true)
            setupButtonsWithPage(0)
        case 2*view.frame.width:
            scrollView.setContentOffset(CGPoint(x: view.frame.width, y: scrollView.contentOffset.y), animated: true)
            setupButtonsWithPage(1)
        default:
            break
        }
    }
    
    @IBAction func onClickNeedHelp(_ sender: Any) {
        
        showAlertWithMessage(message: ALERT_NEED_HELP_MESSAGE, title: ALERT_NEED_HELP_TITLE)
    }
    
    @IBAction func onClickSkip(_ sender: Any) {
        
        openTabBarControllerFirstTime()
    }
}

extension HelpViewController: UIScrollViewDelegate {
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if(scrollView.contentOffset.x > 0 && scrollView.contentOffset.x < scrollView.frame.width) {
            
            scrollView.contentOffset.x = 0
        }
        if(scrollView.contentOffset.x < scrollView.frame.width * 2 && scrollView.contentOffset.x > scrollView.frame.width) {
            
            scrollView.contentOffset.x = scrollView.frame.width
        }
        if(scrollView == self.scrollView) {
            
            setupButtonsWithPage(Int(scrollView.contentOffset.x/view.frame.size.width))
        }
    }
}
