//
//  LoadingViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/31/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreBluetooth

class LoadingViewController: BaseViewController {
    
    var vmDevice:VMDevice?
    let serviceDevice = ServiceDevice()
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + SPLSH_SHOWING_TIME) {
            
            self.setupMainController()
        }
    }
    
    
    // MARK: - Private methods
    private func setupMainController() {
        
        let helpAlreadyOpened:Bool = UserDefaults.standard.bool(forKey: HELP_ALREADY_OPENED)
        if(helpAlreadyOpened) {

            openTabBarController()
        }
        else {

            openHelpController()
        }
    }
    
    private func openHelpController() {
        
        let storyboard = UIStoryboard.init(name: "Help", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "HelpViewController")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = controller
    }
}
