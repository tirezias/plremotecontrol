//
//  MapViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/23/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: BaseViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var gateContainerView: UIView!
    @IBOutlet weak var imageViewGate: UIImageView!
    @IBOutlet weak var labelDeviceName: UILabel!
    @IBOutlet weak var labelDeviceLocation: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var constraintGateViewBottom:NSLayoutConstraint!
    
    var arrayGates:[VMGate] = []
    let serviceDevice = ServiceDevice()
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        getGatesFromLocalStorage()
        
        navigationItem.rightBarButtonItem = SettingsBarButtonItem.shared
    }
    
    
    // MARK: - Private methods
    private func setupController() {
        
        navigationItem.rightBarButtonItem = SettingsBarButtonItem.shared
        
        mapView.delegate = self
        mapView.register(DeviceAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
    }
    
    private func getGatesFromLocalStorage() {
        
        arrayGates = serviceDevice.getGatesFromLocalStorage()
        mapView.addAnnotations(arrayGates)
        zoomAndCenterMap(0.7)
    }
    
    private func zoomAndCenterMap(_ zoom: Double) {
        
        if(mapView.annotations.count > 0) {
         
            let annotation = mapView.annotations.first!
            var span: MKCoordinateSpan = mapView.region.span
            span.latitudeDelta = zoom
            span.longitudeDelta = zoom
            let region: MKCoordinateRegion = MKCoordinateRegion(center: annotation.coordinate, span: span)
            mapView.setRegion(region, animated: false)
        }
    }
    
    private func setVisibleAllAnnotations() {
        
        var zoomRect = MKMapRect.null
        for annotation in mapView.annotations {
            
            let annotationPoint = MKMapPoint.init(annotation.coordinate)
            let pointRect = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0, height: 0)
            if (zoomRect.isNull) {
                zoomRect = pointRect
            } else {
                zoomRect = zoomRect.union(pointRect)
            }
        }
        mapView.setVisibleMapRect(zoomRect, edgePadding: MAP_EDGE_INSETS, animated: false)
    }
    
    private func swtupSelecetdGateInfo(viewModel:VMGate) {
        
        imageViewGate.image = viewModel.MHz_300 ? UIImage(named: "plremote300") : UIImage(named: "plremote310")
        labelDeviceName.text = viewModel.name
        if(viewModel.location != nil) {
                
            labelDeviceLocation.text = viewModel.location!.street
        }
        startUpdatingLocation(viewModel:viewModel)
    }
    
    private func showGateInfoView(_ viewModel:VMGate) {
        
        constraintGateViewBottom.constant = 16
        swtupSelecetdGateInfo(viewModel: viewModel)
        UIView.animate(withDuration: 0.3, animations: {
            
            self.view.layoutIfNeeded()
        }, completion: { (finish) in
            
            if(finish) {
            
            }
        })
    }
    
    private func hideGateInfo() {
        
        constraintGateViewBottom.constant = -100
        UIView.animate(withDuration: 0.3, animations: {
            
            self.view.layoutIfNeeded()
        }, completion: { (finish) in
            
            if(finish) {
            
            }
        })
    }
    
    private func  startUpdatingLocation(viewModel:VMGate) {
                 
     if (LocationManager.sharedInstance.latestLocation != nil) {
         
         LocationManager.sharedInstance.registerForHeading { [weak self] (value) in
             
            if(viewModel.location != nil) {
                
                if (viewModel.location!.latitude != 0 && viewModel.location!.longitude != 0) {
                                    
                        let location = CLLocation(latitude: (viewModel.location!.latitude), longitude: (viewModel.location!.longitude))
                        let string = LocationManager.sharedInstance.latestLocation?.distance(from: location)
                        if var number = string {
                            
                            var result = ""
                            number = number / MYLE
                            result = number.roundTo(places: 1).cleanValue()+"Mi".localized
                            self?.labelDistance.text = "  "+result+"  "
                        }
                    }
                }
            }
        }
     }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if(view.isKind(of: DeviceAnnotationView.self)) {
            
            let deviceGate:DeviceAnnotationView = view as! DeviceAnnotationView
            let vmGate:VMGate = deviceGate.annotation as! VMGate
            deviceGate.image = UIImage(named: "iconAnnotationSelected")
            showGateInfoView(vmGate)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
        if(view.isKind(of: DeviceAnnotationView.self)) {
            
            let deviceGate:DeviceAnnotationView = view as! DeviceAnnotationView
            deviceGate.image = UIImage(named: "iconAnnotation")
            hideGateInfo()
        }
    }
}
