//
//  GatesListTableViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/23/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreLocation

class GatesListTableViewController: BaseTableViewController {

    var dataSource:TableViewDataSource!
    let serviceDevice = ServiceDevice()
    var templocation:CLLocation?
    var reloadDistance = 0.0
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupTableView()
        getGatesFromLocalStorage()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "Gate-EditGate") {
            
            let viewController:DeviceDetailsViewController = segue.destination as! DeviceDetailsViewController
            viewController.vmGate = sender as? VMGate
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteButton = UITableViewRowAction(style: .default, title: "Delete") { (action, indexPath) in
            
            self.tableView.dataSource?.tableView!(self.tableView, commit: .delete, forRowAt: indexPath)
            return
        }
        deleteButton.backgroundColor = UIColor.black
        return [deleteButton]
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let vmGate:VMGate = dataSource.items[indexPath.row] as! VMGate
        let deleteAction = UIContextualAction(style: .normal, title: "") { (action, view, completion) in
          
            self.showAlertForDeleteAction(vmGate: vmGate, indexPath: indexPath)
            completion(true)
        }
        deleteAction.backgroundColor = UIColor(hexString: "FAFBFE")
        deleteAction.image = UIImage(named: "iconDelete")

        let editAction = UIContextualAction(style: .normal, title: "") { (action, view, completion) in

            self.performSegue(withIdentifier: "Gate-EditGate", sender: vmGate)
            completion(true)
        }
        editAction.backgroundColor = UIColor(hexString: "FAFBFE")
        editAction.image = UIImage(named: "iconEdit")
        
        return UISwipeActionsConfiguration(actions: [deleteAction, editAction])
    }
    
    
    // MARK: - Private methods
    private func setupTableView() {
        
        let configureCell:(TableViewCellBlock) = {cell, item, edited in
            cell.configureCell(item: item)
        }
        dataSource = TableViewDataSource([], aCellIdentifier: CellIdentifierGate, aConfigureCellBlock: configureCell)
        dataSource.isEditing = true
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
    
    private func getGatesFromLocalStorage() {
        
        dataSource.items = serviceDevice.getGatesFromLocalStorage()
        updateCellPlaces()
        
        tableView.reloadData()
    }

    private func updateCellPlaces() {
        
         var dataArray:[VMGate]? = serviceDevice.getGatesFromLocalStorage()
           
        if (dataArray?.count ?? 0 > 1) {
               
               if (LocationManager.sharedInstance.latestLocation != nil) {
                  
                   LocationManager.sharedInstance.registerForHeading { [weak self] (value) in
                       
                       if (self?.templocation == nil) {
                           
                           self?.templocation = LocationManager.sharedInstance.latestLocation
                       }
                       if (Double((LocationManager.sharedInstance.latestLocation?.distance(from: ((self?.templocation ?? CLLocation(latitude: 0,longitude: 0)))) ?? 0.0)) >=  self?.reloadDistance ?? 0) {
                           
                           if (dataArray != nil) {
                               
                               for obj in dataArray! {
                                   
                                   let location:CLLocation = CLLocation(latitude: obj.location!.latitude, longitude: obj.location!.longitude)
                                   let dist = LocationManager.sharedInstance.latestLocation?.distance(from: location)
                                   obj.distance = Double(dist!)
                               }
                               dataArray!.sort(by: { (p1, p2) -> Bool in
                                   
                                   return p1.distance < p2.distance
                               })
                               self?.reloadDistance = 20
                               self?.templocation = nil
                               self?.dataSource.items = dataArray
                           }
                       }
                   }
               }
           }
    }

    private func showAlertForDeleteAction(vmGate:VMGate, indexPath: IndexPath) {
        
        let alertController: UIAlertController = UIAlertController(title: "", message: "Are you sure you want to delete controller?", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in}
        let deleteAction: UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { action -> Void in
             
            self.serviceDevice.deleteGate(vmGate)
            self.dataSource.items.remove(at: indexPath.row)
            self.tableView.reloadData()
            
        }
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
