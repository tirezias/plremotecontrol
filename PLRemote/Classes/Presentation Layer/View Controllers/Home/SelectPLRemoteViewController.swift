//
//  SelectPLRemoteViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 9/17/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreBluetooth

class SelectPLRemoteViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewContainerTable: UIView!
    @IBOutlet weak var labelNothingFound: UILabel!
    @IBOutlet weak var constraitHeightContainer:NSLayoutConstraint!
    
    var scanTimer:Timer?
    var dataSource:TableViewDataSource!
    var addingNewDevice:Bool = false
    let ble = BLE()
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupUI()
        setupTableView()
        setupNotifications()
        setupBLE()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Private methods
    private func setupUI() {
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        viewContainerTable.setShadow()
        constraitHeightContainer.constant = 0
    }
    
    private func setupTableView() {
        
        let configureCell:(TableViewCellBlock) = {cell, item, edited in
            cell.configureCell(item: item)
        }
        dataSource = TableViewDataSource.init([], aCellIdentifier:CellIdentifierNearDevices, aConfigureCellBlock: configureCell)
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
    
    private func setupNotifications() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.newDeviceFound(notification:)),
                                               name: NSNotification.Name(rawValue: DEVICE_FOUND),
                                               object: nil)
 
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.connectionState(notification:)),
                                               name: NSNotification.Name(rawValue: CONNECTION_STATE),
                                               object: nil)
    }
    
    private func setupBLE() {
        
        BluetoothManager.shared.setupBLE()
        BluetoothManager.shared.startScan()
    }
    
    @objc
    private func newDeviceFound(notification:Notification) {
        
        finishScanTimer()
        let dict = notification.userInfo
        let viewModel:VMDevice = dict![DEVICE_FOUND] as! VMDevice
        let arrayDevices = dataSource.items as! [VMDevice]
        if(addingNewDevice) {
            
            let serviceDevice = ServiceDevice()
            let arraySavedDevices = serviceDevice.getConnectedDevices()
            if (!arraySavedDevices.contains(where: { $0.identifier == viewModel.identifier })) {
             
                dataSource.items.append(viewModel)
                labelNothingFound.isHidden = true
                tableView.reloadData()
                calculateContainerHeight()
                labelNothingFound.isHidden = true
            }
            else {
                
                labelNothingFound.isHidden = false
            }
        }
        else {
         
            if (!arrayDevices.contains(where: { $0.identifier == viewModel.identifier })) {
             
                dataSource.items.append(viewModel)
                labelNothingFound.isHidden = true
                tableView.reloadData()
                calculateContainerHeight()
            }
            else {
                
                labelNothingFound.isHidden = false
            }
        }
    }
    
    @objc
    private func connectionState(notification:Notification) {
        
        let dict = notification.userInfo
        let state:Bool = dict![CONNECTION_STATE] as! Bool
        hideActivityIndicator()
        if(state) {
         
            if(addingNewDevice) {
                
                navigationController?.popViewController(animated: true)
            }
            else {
             
                performSegue(withIdentifier: "SelectDevice-Finish", sender: nil)
            }
        }
    }
    
    @objc
    private func finishScanTimer() {
        
        hideActivityIndicator()
        labelNothingFound.isHidden = dataSource.items.count > 0 ? true : false
        if(scanTimer != nil) {
                
            scanTimer!.invalidate()
            scanTimer = nil
        }
    }
    
    private func calculateContainerHeight() {
        
        constraitHeightContainer.constant = dataSource.items.count > 0 ? CGFloat(20 + dataSource.items.count * 50) : 0
    }
    
    
    // MARK: - Action methods
    @IBAction func onClickScan(_ sender: Any) {
    
        if(scanTimer == nil) {

            scanTimer = Timer.scheduledTimer(timeInterval: SCAN_TIME, target: self, selector: #selector(finishScanTimer), userInfo: nil, repeats: false)
        }
        showActivityIndicator()
        dataSource.items.removeAll()
        tableView.reloadData()
        calculateContainerHeight()

        BluetoothManager.shared.stopScan()
        BluetoothManager.shared.startScan()
    }
}

extension SelectPLRemoteViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        showActivityIndicator()
        let viewModel:VMDevice = dataSource.items[indexPath.row] as! VMDevice
        viewModel.isSelected = true
        tableView.reloadData()
        BluetoothManager.shared.vmDevice = viewModel
        BluetoothManager.shared.connect(viewModel)
    }
}
