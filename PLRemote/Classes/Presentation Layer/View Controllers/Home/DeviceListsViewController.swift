//
//  DeviceListsViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/31/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreLocation
import CoreBluetooth

class DeviceListsViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, DeviceDetailsViewControllerDelegate, PultCollectionViewCellDelegate {
    
    var dataArray:[VMGate] = []
    var pultCollectionView:UICollectionView?
    var rightBarButton = UIBarButtonItem()
    var editMode = false
    var addButton = UIButton()
    var editedGateIndex:Int?
    var transmitedVMGate:VMGate?
    var hintLabel = UILabel()
    var templocation:CLLocation?
    var reloadDistance = 0.0
    let refreshControl = UIRefreshControl()
    let serviceDevice = ServiceDevice()
    
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        LocationManager.sharedInstance.start()
        setupCollectionView()
        setupController()
        setupNotifications()
        LocationManager.sharedInstance.registerForLocation { [weak self] (location) in
            
            LocationManager.sharedInstance.callback = nil
            self!.updateCellPlaces()
            
        }
        if UserDefaults.standard.integer(forKey: "doWeSeeHint") < 1
        {
            self.tabBarController?.tabBar.barTintColor = UIColor(hexString: "727272")
            self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "727272")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        checkForHint()
        distanceUpdate()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        
            self.updateCellPlaces()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        makeRequest()
    }
    
    
    // MARK: - Private methods
    private func setupController() {
        
        navigationItem.rightBarButtonItem = SettingsBarButtonItem.shared
        
        hintLabel.frame = CGRect(x: 17, y: 100, width: self.view.frame.size.width - 34, height: 100)
        hintLabel.numberOfLines = 0
        hintLabel.text = "Please add a gate to have ability to open something 😀"
        self.view.addSubview(hintLabel)
    }
    
    private func setupCollectionView() {
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height + (self.navigationController?.navigationBar.frame.height ?? 0.0)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: self.view.frame.size.width - 10,
                                 height: self.view.frame.size.height - topBarHeight - (self.tabBarController?.tabBar.frame.height ?? 0.0) - 10 )
        
        pultCollectionView = UICollectionView(frame: CGRect(x: 0,
                                                            y: topBarHeight ,
                                                            width: self.view.frame.size.width,
                                                            height: layout.itemSize.height + 10),
                                              collectionViewLayout: layout)
        pultCollectionView?.dataSource = self
        pultCollectionView?.delegate = self
        pultCollectionView?.isPagingEnabled = true
        pultCollectionView?.register(PultCollectionViewCell.self, forCellWithReuseIdentifier: "pultCell")
        view.addSubview(pultCollectionView!)
        
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        pultCollectionView?.refreshControl = refreshControl
    }
    
    private func setupNotifications() {
    
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.planUpdate(notification:)),
                                               name: NSNotification.Name(rawValue: PLAN_UPGRADE),
                                               object: nil)
    }
    
    private func makeRequest() {
        
        let serviceDevice = ServiceDevice()
        dataArray = serviceDevice.getGatesFromLocalStorage()
    }
    
    private func startTransmit(vmGate:VMGate) {
        
        var frequency:BLE.Frequency = .F300
        if(vmGate.MHz_300) {
            
            frequency = .F300
            _ = BluetoothManager.shared.ble.startTransmit(data: vmGate.data, frequency: frequency, dipSwitchCount: 8)
        }
        else if(vmGate.MHz_310) {
            
            frequency = .F310
            _ = BluetoothManager.shared.ble.startTransmit(data: vmGate.data, frequency: frequency, dipSwitchCount: 10)
        }
    }
    
    private func showAlertForDeleteAction(vmGate:VMGate) {
        
        let alertController: UIAlertController = UIAlertController(title: "", message: "Are you sure you want to delete controller?", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in}
        let deleteAction: UIAlertAction = UIAlertAction(title: "Yes", style: .destructive) { action -> Void in
             
            self.serviceDevice.deleteGate(vmGate)
            self.showActivityIndicator()
            self.hideActivityIndicator()
            self.gateDeleted()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc
    private func refreshData(_ sender: Any) {
        
        distanceUpdate()
    }
    
    @objc
    private func planUpdate(notification:Notification) {
        
        pultCollectionView?.reloadData()
    }
    
    @objc
    func navBarButtonTapped() {
        
        if editMode == false {
            
            editMode = true
            rightBarButton.title = "Done"
            for cell in pultCollectionView!.visibleCells {
                
                let customCell: PultCollectionViewCell = cell as! PultCollectionViewCell
                customCell.startAnimate()
            }
        }
        else {
            
            editMode = false
            rightBarButton.title = "Edit"
            for cell in pultCollectionView!.visibleCells {
                
                let customCell: PultCollectionViewCell = cell as! PultCollectionViewCell
                customCell.stopAnimate()
            }
        }
    }
    
    
    // MARK: - Action methods
    @objc
    func addAction(sender: UIButton!) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let deviceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "DeviceDetailsViewController") as! DeviceDetailsViewController
        deviceDetailsViewController.delegate = self
        navigationController?.pushViewController(deviceDetailsViewController, animated: true)
    }
    
    func editGate(_ vmGate:VMGate) {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let deviceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "DeviceDetailsViewController") as! DeviceDetailsViewController
        deviceDetailsViewController.delegate = self
        deviceDetailsViewController.vmGate = vmGate
        navigationController?.pushViewController(deviceDetailsViewController, animated: true)
    }
    
    
    // MARK: - UICollectionViewDelegate and UICollectionViewDatasource methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if serviceDevice.getAvailableGateCount() < dataArray.count
        {
             return serviceDevice.getAvailableGateCount()
        }
        return dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pultCell", for: indexPath) as! PultCollectionViewCell
        cell.layer.cornerRadius = 5
        cell.backgroundColor = UIColor.white
        
        let vmGate:VMGate = dataArray[indexPath.row]
        cell.vmGate = vmGate
        cell.delegate = self
        if (vmGate.name.count > 0) {
            
            cell.nameLabel.text = vmGate.name
            cell.addressLabel.text = vmGate.location!.street
            cell.addressLabel.sizeToFit()
            if (cell.addressLabel.frame.width > cell.frame.size.width - 130) {
                
                cell.addressLabel.frame = CGRect(x: 50, y: 78 , width: cell.frame.size.width - 130, height: 20)
            }
            cell.addressLabel.center = CGPoint(x: cell.frame.size.width / 2 - 12, y: cell.addressLabel.center.y)
            cell.locationLabel.frame = CGRect(x: cell.addressLabel.frame.origin.x + cell.addressLabel.frame.size.width + 5, y: 78, width: 150, height: 21)
            cell.locationImageView.center = CGPoint(x: cell.addressLabel.frame.origin.x - 20, y: cell.locationImageView.center.y)
        }
        else {
            
            cell.addressLabel.text = vmGate.location!.street
            cell.addressLabel.sizeToFit()
        }
        cell.pultImageView.image = vmGate.MHz_300 ? UIImage(named: "plremote300") : UIImage(named: "plremote310")
        cell.emptyPultImageView.image = vmGate.MHz_300 ? UIImage(named: "plremote300empty") : UIImage(named: "plremote310empty")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        if editMode == true {

            (cell as! PultCollectionViewCell).startAnimate()
        }
        else {

            (cell as! PultCollectionViewCell).stopAnimate()
        }
   }
    
    
    // MARK: - DeviceDetailsViewControllerDelegate methods
    func newGateAdded(_ vmGate:VMGate) {
        
        dataArray.insert(vmGate, at: 0)
        pultCollectionView?.reloadData()
    }
    
    func gateEdited(_ vmGate:VMGate) {
        
        if(editedGateIndex != nil) {
            
            dataArray[editedGateIndex!] = vmGate
            pultCollectionView?.reloadData()
        }
    }
    
    func gateDeleted() {
        
        makeRequest()
        pultCollectionView?.reloadData()
    }
    
    
    // MARK: - PultCollectionViewCellDelegate methods
    func onLongPressCell(vmGate:VMGate) {
        
        if(BluetoothManager.shared.isConnected) {
         
            let generator = UINotificationFeedbackGenerator()
            generator.notificationOccurred(.success)
            startTransmit(vmGate: vmGate)
            for viewModel in dataArray {
                
                if(viewModel.id == vmGate.id) {
                   
                    viewModel.isTransmited = true
                }
                else {
                    
                    viewModel.isTransmited = false
                }
            }
            self.tabBarController?.tabBar.barTintColor = UIColor.white
            self.navigationController?.navigationBar.barTintColor = UIColor.white
        }
        else {
         
            showAlertWithMessage(message: "PL Remote is not in range", title: "")
            pultCollectionView!.reloadData()
        }
    }
    
    func onLongPressCellEnded(vmGate:VMGate) {
        
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
            
            _ = BluetoothManager.shared.ble.endTransmit()
        })
    }
    
    func onClickSettings(vmGate:VMGate) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in }
        actionSheetController.addAction(cancelActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Delete", style: .default) { _ in
            
            self.showAlertForDeleteAction(vmGate: vmGate)
        }
        deleteActionButton.setValue(UIColor.red, forKey: "titleTextColor")
        
        let saveActionButton = UIAlertAction(title: "Edit", style: .default) { _ in
            
            self.editGate(vmGate)
        }
        actionSheetController.addAction(saveActionButton)
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func checkForHint() {
        
        if (dataArray.count == 0) {
            
            hintLabel.isHidden = false
            pultCollectionView?.backgroundColor = UIColor.rgb(233, green: 233, blue: 233)
        }
        else {
            
            hintLabel.isHidden = true
            pultCollectionView?.backgroundColor = UIColor.rgb(233, green: 233, blue: 233)
        }
    }
    
    func updateCellPlaces() {
        
        if (dataArray.count > 1) {
            
            if (LocationManager.sharedInstance.latestLocation != nil) {
               
                LocationManager.sharedInstance.registerForHeading { [weak self] (value) in
                    
                    if (self?.templocation == nil) {
                        
                        self?.templocation = LocationManager.sharedInstance.latestLocation
                    }
                    if (Double((LocationManager.sharedInstance.latestLocation?.distance(from: ((self?.templocation ?? CLLocation(latitude: 0,longitude: 0)))) ?? 0.0)) >=  self?.reloadDistance ?? 0) {
                        
                        if (self?.dataArray != nil) {
                            
                            for obj in self!.dataArray {
                                
                                let location:CLLocation = CLLocation(latitude: obj.location!.latitude, longitude: obj.location!.longitude)
                                let dist = LocationManager.sharedInstance.latestLocation?.distance(from: location)
                                obj.distance = Double(dist!)
                            }
                            self?.dataArray.sort(by: { (p1, p2) -> Bool in
                                
                                return p1.distance < p2.distance
                            })
                            self?.reloadDistance = 20
                            self?.templocation = nil
                            self?.pultCollectionView?.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func distanceUpdate() {
        
        for obj in dataArray {
         
            let location:CLLocation = CLLocation(latitude: obj.location!.latitude, longitude: obj.location!.longitude)
            if let dist = LocationManager.sharedInstance.latestLocation?.distance(from: location) {
                
                obj.distance = Double(dist)
            }
        }
        self.dataArray.sort(by: { (p1, p2) -> Bool in
            
            return p1.distance < p2.distance
        })
        self.pultCollectionView?.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            
            self.refreshControl.endRefreshing()
        })
    }
}
