//
//  FinishViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/22/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class FinishViewController: BaseViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func onClickFinish(_ sender: Any) {
        
        openTabBarControllerFirstTime()
    }
}
