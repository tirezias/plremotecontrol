//
//  DeviceStatusViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/29/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class DeviceStatusViewController: BaseViewController {

    @IBOutlet weak var containerViewConnected: UIView!
    @IBOutlet weak var containerViewDisconnected: UIView!
    
    var vmDevice:VMDevice!
    let serviceDevice = ServiceDevice()
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupController()
        setupNotifications()
    }
    
    
    // MARK: - Private methods
    private func setupController() {
        
        navigationItem.title = vmDevice.deviceName
        containerViewConnected.isHidden = !vmDevice.isConnected
        containerViewDisconnected.isHidden = vmDevice.isConnected
        
        if(vmDevice.isConnected) {
            
            containerViewConnected.setShadow()
        }
        else {
            
            containerViewDisconnected.setShadow()
        }
    }
    
    private func setupNotifications() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.popViewController),
                                               name: NSNotification.Name(rawValue: CONNECTION_STATE),
                                               object: nil)
    }
    
    @objc func popViewController() {
        
        navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Action methods
    @IBAction func onClickConnect(_ sender: Any) {
        
        BluetoothManager.shared.disconnect()
        BluetoothManager.shared.vmDevice = vmDevice
        BluetoothManager.shared.startScan()
    }
    
    @IBAction func onClickForgetDevice(_ sender: Any) {
        
        serviceDevice.removeDeviceFromLocalStorage(identifier: vmDevice.identifier)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDisconnect(_ sender: Any) {
        
        BluetoothManager.shared.disconnect()
    }
}
