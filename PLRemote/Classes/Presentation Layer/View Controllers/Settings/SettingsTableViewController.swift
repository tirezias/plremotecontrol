//
//  SettingsTableViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/25/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class SettingsTableViewController: BaseTableViewController {

    @IBOutlet weak var batteryBackgroundView: UIView!
    @IBOutlet weak var myDevicesBackgroundView: UIView!
    @IBOutlet weak var purchaseBackgroundView: UIView!
    @IBOutlet weak var aboutBackgroundView: UIView!
    @IBOutlet weak var batteryPercentLabel: UILabel!
    @IBOutlet weak var labelFirmwareVersion: UILabel!
    
    var dataSource:TableViewDataSource!
    let serviceDevice = ServiceDevice()
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        configureViews()
        setupTableView()
        setupNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        getGatesFromLocalStorage()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "DeviceList-DeviseStatus") {
            
            let viewController:DeviceStatusViewController = segue.destination as! DeviceStatusViewController
            viewController.vmDevice = sender as? VMDevice
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if(indexPath.row == 0) {

            return 64
        }
        else {

            return 70
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row == dataSource.items.count - 1) {
            
            addNewDeviceCellClicked()
        }
        else {
            
            let vmDevice:VMDevice = dataSource.items[indexPath.row] as! VMDevice
            performSegue(withIdentifier: "DeviceList-DeviseStatus", sender: vmDevice)
        }
    }
    
    
    // MARK: - Private methods
    private func configureViews() {
        
        batteryBackgroundView.setShadow()
        purchaseBackgroundView.setShadow()
        aboutBackgroundView.setShadow()
    }
    
    private func setupTableView() {
        
        let configureCell:(TableViewCellBlock) = {cell, item, edited in
            cell.configureCell(item: item)
        }
        dataSource = TableViewDataSource([], aCellIdentifier: CellIdentifierMyDevice, aConfigureCellBlock: configureCell)
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
    
    private func setupNotifications() {
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.getGatesFromLocalStorage),
                                               name: NSNotification.Name(rawValue: CONNECTION_STATE),
                                               object: nil)
    }
    
    @objc
    private func getGatesFromLocalStorage() {
        
        let arrayDevices = serviceDevice.getConnectedDevices()
        dataSource.items = arrayDevices
        tableView.reloadData()
        
        batteryPercentLabel.text = BluetoothManager.shared.isConnected ? "100%" : "N/A"
        labelFirmwareVersion.text = BluetoothManager.shared.isConnected ? FIRMWARE_VERSION : "N/A"
    }
    
    private func addNewDeviceCellClicked() {
     
        let storyboard = UIStoryboard.init(name: "NewDevice", bundle: nil)
        let controller:SelectPLRemoteViewController = storyboard.instantiateViewController(withIdentifier: "SelectPLRemoteViewController") as! SelectPLRemoteViewController
        controller.addingNewDevice = true
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    // MARK: - Action methods
    @IBAction func updatePlanCellClicked(_ sender: Any) {
        
    }
    
    @IBAction func termsClicked(_ sender: Any) {
        let vc = WebViewVC()
               vc.hideNavigation = true
               vc.url = "http://plremote.tech/terms-of-use"
               vc.navTitle = "Terms of use"
               present(vc, animated: true, completion: nil)
    }
    @IBAction func privacyClicked(_ sender: Any) {
        let vc = WebViewVC()
        vc.hideNavigation = true
        vc.url = "http://plremote.tech/privacy-policy"
        vc.navTitle = "Privacy policy"
        present(vc, animated: true, completion: nil)
    }
}
