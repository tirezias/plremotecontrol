//
//  IAPViewController.swift
//  PLRemote
//
//  Created by Ishkhan Gevorgyan on 6/25/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class IAPViewController: UIViewController {

    @IBOutlet weak var bronseBackgroundView: UIView!
    @IBOutlet weak var silverBackgroundView: UIView!
    @IBOutlet weak var goldBackgroundView: UIView!
        
    @IBOutlet weak var bronseRectView: UIImageView!
    @IBOutlet weak var silverRectView: UIImageView!
    @IBOutlet weak var goldRectView: UIImageView!
    
    @IBOutlet weak var buttonBronse: UIButton!
    @IBOutlet weak var buttonSilver: UIButton!
    @IBOutlet weak var buttonGold: UIButton!
    
    var canMakeAClick = true
    let serviceDevice = ServiceDevice()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        purchse()
        getPurchasedStatus()
    }
    
    func configureViews() {
       
        bronseBackgroundView.setShadow()
        silverBackgroundView.setShadow()
        goldBackgroundView.setShadow()
    }
    
    func purchse() {
        
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            
            guard self != nil else { return }
            
            if (type == .purchased) {
                
                self?.getPurchasedStatus()
                self?.navigationController?.popToRootViewController(animated: true)
            }
            self?.hideActivityIndicator()
        }
    }
    
    func getPurchasedStatus() {
        
        bronseRectView.isHidden = true
        silverRectView.isHidden = true
        goldRectView.isHidden = true
        
        switch serviceDevice.getAvailableGateCount() {
        case 10:
            bronseRectView.isHidden = false
        case 25:
            silverRectView.isHidden = false
        case 100000:
            goldRectView.isHidden = false
        default:
            break
        }
    }
    
    
    // MARK: - Action methods
    // MARK: - Bronse methods
    @IBAction func bronsePlanClicked(_ sender: Any) {
        
        if canMakeAClick == true
        {
            canMakeAClick = false
            IAPHandler.shared.purchaseMyProduct(index: 0)
            buttonBronse.backgroundColor = .clear
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                            
                self.canMakeAClick = true
            })
        }
    }
    
    @IBAction func buttonBronzeTouchDown(_ sender: Any) {
        if canMakeAClick == true
        {
            buttonBronse.backgroundColor = UIColor(named: "Light Gray")
            buttonBronse.alpha = 0.5
        }
    }
    
    @IBAction func buttonBronzeTouchUp(_ sender: Any) {
        
        buttonBronse.backgroundColor = .clear
    }
    
    
    // MARK: - Silver methods
    @IBAction func silverPlanClicked(_ sender: Any) {
        if canMakeAClick == true
        {
            canMakeAClick = false
           IAPHandler.shared.purchaseMyProduct(index: 1)
            buttonSilver.backgroundColor = .clear
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                            
                self.canMakeAClick = true
            })
        }
    }
    
    @IBAction func buttonSilverTouchDown(_ sender: Any) {
        if canMakeAClick == true
        {
            buttonSilver.backgroundColor = UIColor(named: "Light Gray")
            buttonSilver.alpha = 0.5
        }
    }
    
    @IBAction func buttonSilverTouchUp(_ sender: Any) {
        
        buttonSilver.backgroundColor = .clear
    }
    
    
    // MARK: - Gold methods
    @IBAction func goldPlanClicked(_ sender: Any) {
        if canMakeAClick == true
        {
            canMakeAClick = false
           IAPHandler.shared.purchaseMyProduct(index: 2)
           buttonGold.backgroundColor = .clear
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                            
                self.canMakeAClick = true
            })
        }
       
    }
    
    @IBAction func buttonGoldTouchDown(_ sender: Any) {
        if canMakeAClick == true
        {
            buttonGold.backgroundColor = UIColor(named: "Light Gray")
            buttonGold.alpha = 0.5
        }
    }
    
    @IBAction func buttonGoldTouchUp(_ sender: Any) {
        
        buttonGold.backgroundColor = .clear
    }
    
    @IBAction func restoreClicked(_ sender: Any) {
        
    }
}


