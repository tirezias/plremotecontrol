//
//  WebViewVC.swift
//  MyCoachCycling
//
//  Created by narek on 4/14/17.
//  Copyright © 2017 e-Works. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController, WKNavigationDelegate {

    var url:String = ""
    var navTitle:String = ""
    var webView: WKWebView!
    var hideNavigation = false
    var doneButton = UIButton()
    var navTitleLabel = UILabel()
    var lineView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
      
           
        // Do any additional setup after loading the view.
        webView = WKWebView(frame: CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height + 44, width: self.view.frame.size.width, height: UIScreen.main.bounds.height - UIApplication.shared.statusBarFrame.height - 44))
        webView.backgroundColor = UIColor.white
         webView.navigationDelegate = self
        self.view.addSubview(webView)
       /// UIApplication.shared.statusBarStyle = .default
        doneButton = UIButton(frame: CGRect(x: 10, y: UIApplication.shared.statusBarFrame.height + 10, width: 50, height: 22))
        
        doneButton.setTitle("Back".localized, for: .normal)
        doneButton.setTitleColor(UIColor.gray, for: .normal)
        doneButton.sizeToFit()
        doneButton.addTarget(self, action: #selector(done), for: .touchUpInside)
        
        self.view.addSubview(doneButton)
        
        navTitleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 22))
        navTitleLabel.center = CGPoint(x: self.view.frame.size.width / 2, y: UIApplication.shared.statusBarFrame.height + 23)
        navTitleLabel.textAlignment = .center
        navTitleLabel.text = navTitle
        navTitleLabel.textColor = UIColor.gray
       
        self.view.addSubview(navTitleLabel)
        
        lineView = UIView(frame: CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height + 44, width: 2000, height: 0.5))
        lineView.backgroundColor = .gray
        self.view.addSubview(lineView)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadUrl()
    }
    
    func loadUrl()
    {
        if url.count > 0
        {
      
            if Connectivity.isConnectedToInternet()
            {
                if let tempUrl = URL(string:url)
                {
                   webView.load(URLRequest(url: tempUrl))
                    webView.allowsBackForwardNavigationGestures = true
                }
            }
            else
            {
            let alert = UIAlertController(title: "NoInternetConnection".localized, message: "NoInternetConnectionDescription".localized, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "retry".localized, style: .default, handler: { action in
              self.loadUrl()
            }))
                alert.addAction(UIAlertAction(title: "back".localized, style: .default, handler: { action in
                   self.done()
                }))
            self.present(alert, animated: true)
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if hideNavigation == true
        {
            self.navigationController?.isNavigationBarHidden = true
        }
       // UIApplication.shared.statusBarStyle = .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func done() {
        self.dismiss(animated: true, completion: nil)
    }

}
