//
//  AddDeviceViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/23/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class AddDeviceViewController: BaseViewController {

    @IBOutlet weak var updatePlanView: UIView!
    @IBOutlet weak var labelPickARemote: UILabel!
    @IBOutlet weak var buttonPurchase: UIButton!
    
    var vmGate:VMGate!
    let serviceDevice = ServiceDevice()
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupController()
        setupNotifications()   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        vmGate = VMGate()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "New_Device-Details") {
            
            let viewController:DeviceDetailsViewController = segue.destination as! DeviceDetailsViewController
            viewController.vmGate = sender as? VMGate
        }
    }
    
    
    // MARK: - Private methods
    private func setupController() {
        
        navigationItem.rightBarButtonItem = SettingsBarButtonItem.shared
        
        updatePlanView.setShadow()
        if (serviceDevice.getGatesCount() >= serviceDevice.getAvailableGateCount()) {
            
            updatePlanView.isHidden = false
            labelPickARemote.text = "You are using all available devices, please update your subscription to add device"
        }
        else {
            
            updatePlanView.isHidden = true
            if serviceDevice.getGatesCount() < 1
            {
                labelPickARemote.text = "Please pick a remote style to add"
            }
            else {
                
                let tempText1  = "You are using " + String(serviceDevice.getGatesCount()) + " of "
                var tempText2  = ""
                if serviceDevice.getAvailableGateCount() != 100000
                {
                    tempText2  =  String(serviceDevice.getAvailableGateCount()) + " remote controllers"
                }
                else
                {
                     tempText2  =  "unlimited remote controllers"
                }
                labelPickARemote.text = tempText1 + tempText2
            }
        }
    }
    
    private func openHelpController() {
    
        let storyboard = UIStoryboard.init(name: "Settings", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "IAPViewController")
        navigationController?.pushViewController(controller, animated: true)
    }
    
    private func setupNotifications() {
    
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.planUpdate(notification:)),
                                               name: NSNotification.Name(rawValue: PLAN_UPGRADE),
                                               object: nil)
    }
    
    @objc
    private func planUpdate(notification:Notification) {
        
        setupController()
    }
    
    
    // MARK: - Action methods
    @IBAction func onClick_10_Dip(_ sender: Any) {
     
        if (updatePlanView.isHidden) {
            
            vmGate.MHz_300 = false
            vmGate.MHz_310 = true
            performSegue(withIdentifier: "New_Device-Details", sender: vmGate)
        }
        else {
            
            let alertController: UIAlertController = UIAlertController(title: ALERT_UPDATE_PlAN_TITLE, message: ALERT_UPDATE_PlAN_MESSAGE, preferredStyle: .alert)
            let cancelButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in}
            let upgradeButton: UIAlertAction = UIAlertAction(title: "Upgrade", style: .default) { action -> Void in
                 
                 self.openHelpController()
            }
            alertController.addAction(cancelButton)
            alertController.addAction(upgradeButton)
            self.present(alertController, animated: true, completion: nil)
         }
    }
    
    @IBAction func onClick_8_Dip(_ sender: Any) {
        
        if (updatePlanView.isHidden) {
            
            vmGate.MHz_300 = true
            vmGate.MHz_310 = false
            performSegue(withIdentifier: "New_Device-Details", sender: vmGate)
        }
        else {
            
            let alertController: UIAlertController = UIAlertController(title: ALERT_UPDATE_PlAN_TITLE, message: ALERT_UPDATE_PlAN_MESSAGE, preferredStyle: .alert)
            let cancelButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in}
            let upgradeButton: UIAlertAction = UIAlertAction(title: "Upgrade", style: .default) { action -> Void in
                
                self.openHelpController()
            }
            alertController.addAction(cancelButton)
            alertController.addAction(upgradeButton)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func purchasePremiumClicked(_ sender: Any) {
        
        openHelpController()
        buttonPurchase.backgroundColor = .clear
    }
    
    @IBAction func buttonPurchaseTouchDown(_ sender: Any) {
        
        buttonPurchase.backgroundColor = UIColor(named: "Light Gray")
        buttonPurchase.alpha = 0.5
    }
    
    @IBAction func buttonPurchaseTouchUp(_ sender: Any) {
        
        buttonPurchase.backgroundColor = .clear
    }
}
