//
//  DeviceDetailsViewController.swift
//  PLRemote
//
//  Created by Admin on 8/31/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreLocation

@objc protocol DeviceDetailsViewControllerDelegate: class {
    
    @objc optional func newGateAdded(_ vmGate:VMGate)
    @objc optional func gateEdited(_ vmGate:VMGate)
    @objc optional func gateDeleted()
}

class DeviceDetailsViewController: BaseTableViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldStreet: UITextField!
    @IBOutlet weak var textFieldCity: UITextField!
    @IBOutlet weak var textFieldState: UITextField!
    @IBOutlet weak var textFieldZip: UITextField!
    
    @IBOutlet weak var confBtn1: UIButton!
    @IBOutlet weak var confBtn2: UIButton!
    @IBOutlet weak var confBtn3: UIButton!
    @IBOutlet weak var confBtn4: UIButton!
    @IBOutlet weak var confBtn5: UIButton!
    @IBOutlet weak var confBtn6: UIButton!
    @IBOutlet weak var confBtn7: UIButton!
    @IBOutlet weak var confBtn8: UIButton!
    @IBOutlet weak var confBtn9: UIButton!
    @IBOutlet weak var confBtn10: UIButton!
    
    @IBOutlet weak var imageViewGate: UIImageView!
    @IBOutlet weak var labelGateType: UILabel!
    @IBOutlet weak var label9: UILabel!
    @IBOutlet weak var label10: UILabel!
            
    weak var delegate:DeviceDetailsViewControllerDelegate?
    
    var isActive = false
    var vmGate:VMGate!
    let serviceDevice = ServiceDevice()
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupController()
       
       
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "DeviceDetails-AddressMap") {
            
            let viewController:AddressMapViewController = segue.destination as! AddressMapViewController
            viewController.delegate = self
        }
    }
    
    // MARK: - Private methods
    private func setupController() {
        
        textFieldName.delegate = self
        textFieldStreet.delegate = self
        textFieldCity.delegate = self
        textFieldState.delegate = self
        textFieldZip.delegate = self
        
        imageViewGate.image = vmGate.MHz_300 ? UIImage(named: "plremote300") : UIImage(named: "plremote310")
        labelGateType.text = vmGate.MHz_300 ? "8 Dip Switch" : "10 Dip Switch"
        confBtn9.isHidden = vmGate.MHz_300 ? true : false
        confBtn10.isHidden = vmGate.MHz_300 ? true : false
        label9.isHidden = vmGate.MHz_300 ? true : false
        label10.isHidden = vmGate.MHz_300 ? true : false
        
        setupGateLocation()
        
        if(vmGate.id != 0) {
            
            self.title = "Edit Remote"
                
            textFieldName.text = vmGate.name
            
            var image:UIImage = vmGate.key1 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn1.setImage(image, for: .normal)
            
            image = vmGate.key2 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn2.setImage(image, for: .normal)
            
            image = vmGate.key3 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn3.setImage(image, for: .normal)
            
            image = vmGate.key4 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn4.setImage(image, for: .normal)
            
            image = vmGate.key5 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn5.setImage(image, for: .normal)
            
            image = vmGate.key6 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn6.setImage(image, for: .normal)
            
            image = vmGate.key7 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn7.setImage(image, for: .normal)
            
            image = vmGate.key8 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn8.setImage(image, for: .normal)
            
            image = vmGate.key9 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn9.setImage(image, for: .normal)
            
            image = vmGate.key10 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn10.setImage(image, for: .normal)
        }
    }
    
    private func setupGateLocation() {
        
        if(vmGate.location != nil) {
            
            textFieldStreet.text = vmGate.location!.street
            textFieldCity.text = vmGate.location!.city
            textFieldState.text = vmGate.location!.country
            textFieldZip.text = vmGate.location!.zip
        }
    }
    
    private func getLocationByAddress() {
        
        let address:String = textFieldStreet.text! + ", " + textFieldCity.text! + ", " + textFieldState.text! + " " + textFieldZip.text!
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            
            if (error == nil) {
                
                if let placemark = placemarks?[0] {
                    
                    let location = placemark.location!
                    self.vmGate.location = VMLocation(latitude: location.coordinate.latitude,
                                                      longitude: location.coordinate.longitude,
                                                      street: self.textFieldStreet.text,
                                                      city: self.textFieldCity.text,
                                                      country: self.textFieldState.text,
                                                      zip: self.textFieldZip.text)
                }
            }
        }
    }
    
    
    // MARK: - Action methods
    @IBAction func configSettings(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            vmGate.key1 = !vmGate.key1
            let image:UIImage = vmGate.key1 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn1.setImage(image, for: .normal)
        case 2:
            vmGate.key2 = !vmGate.key2
            let image:UIImage = vmGate.key2 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn2.setImage(image, for: .normal)
        case 3:
            vmGate.key3 = !vmGate.key3
            let image:UIImage = vmGate.key3 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn3.setImage(image, for: .normal)
        case 4:
            vmGate.key4 = !vmGate.key4
            let image:UIImage = vmGate.key4 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn4.setImage(image, for: .normal)
        case 5:
            vmGate.key5 = !vmGate.key5
            let image:UIImage = vmGate.key5 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn5.setImage(image, for: .normal)
        case 6:
            vmGate.key6 = !vmGate.key6
            let image:UIImage = vmGate.key6 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn6.setImage(image, for: .normal)
        case 7:
            vmGate.key7 = !vmGate.key7
            let image:UIImage = vmGate.key7 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn7.setImage(image, for: .normal)
        case 8:
            vmGate.key8 = !vmGate.key8
            let image:UIImage = vmGate.key8 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn8.setImage(image, for: .normal)
        case 9:
            vmGate.key9 = !vmGate.key9
            let image:UIImage = vmGate.key9 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn9.setImage(image, for: .normal)
        case 10:
            vmGate.key10 = !vmGate.key10
            let image:UIImage = vmGate.key10 ? UIImage(named: "ic_switcher_selected")! : UIImage(named: "ic_switcher")!
            confBtn10.setImage(image, for: .normal)
        default:
            break
        }
    }
    
    @IBAction func onClickSaveDevice(_ sender: UIButton) {
        
        var message = ""

        vmGate.name = textFieldName.text!
        
        if (textFieldName.text == "") {
            
            message += "\n  " + textFieldName.placeholder!
        }
        
        if (textFieldStreet.text == "") {
            
            message += "\n  " + textFieldStreet.placeholder!
        }
        if (textFieldCity.text == "") {
            
            message += "\n  " + textFieldCity.placeholder!
        }
        if (textFieldState.text == "") {
            
            message += "\n  " + textFieldState.placeholder!
        }
        vmGate.calculateData()
        
        if (!message.isEmpty) {
            
            let alert = UIAlertController(title: REQUIRED_FIELDS, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in }))
            self.present(alert, animated: true, completion: nil)
        }
        else {
            
            if(vmGate.location == nil) {
                
                vmGate.location = VMLocation()
            }
            vmGate.location!.street = textFieldStreet.text!
            vmGate.location!.city = textFieldCity.text!
            vmGate.location!.country = textFieldState.text!
            vmGate.location!.zip = textFieldZip.text!
            if(self.vmGate.id == 0) {
                
                let lastGateId:Int = self.serviceDevice.getLastGateId()
                vmGate.id = lastGateId + 1
                serviceDevice.addNewGate(self.vmGate)
            }
            else {
                
                self.serviceDevice.editGate(self.vmGate)
                self.delegate?.gateEdited?(self.vmGate)
            }
            
            openTabBarController()
        }
    }
    
    @IBAction func deleteRemoreClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Do you really want to delete Remote?", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
              
            self.serviceDevice.deleteGate(self.vmGate)
            self.showActivityIndicator()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                
                self.hideActivityIndicator()
                self.delegate?.gateDeleted?()
                self.navigationController?.popViewController(animated: true)
            })
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changeDeviceClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard.init(name: "General", bundle: nil)
        let controller:DevicesActionSheetViewController = storyboard.instantiateViewController(withIdentifier: "DevicesActionSheetViewController") as! DevicesActionSheetViewController
        controller.selectedDevice = vmGate.MHz_310 ? .DIP_10 : .DIP_8
        controller.delegate = self
        controller.modalPresentationStyle = .overCurrentContext
        tabBarController?.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func onClickAddressMap(_ sender: UIButton) {
        
        if Connectivity.isConnectedToInternet() {
            
            DispatchQueue.main.async {
                
                self.view.endEditing(true)
                self.performSegue(withIdentifier: "DeviceDetails-AddressMap", sender: nil)
            }
        }
        else {
            
            DispatchQueue.main.async {
                
                self.showAlertWithMessage(message: ALERT_NEED_CONNECTION_MESSAGE, title: ALERT_NEED_CONNECTION_TITLE)
            }
        }
    }
}

extension DeviceDetailsViewController {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        switch textField {
        case textFieldName:
            textFieldStreet.becomeFirstResponder()
        case textFieldStreet:
            textFieldCity.becomeFirstResponder()
        case textFieldCity:
            textFieldState.becomeFirstResponder()
        case textFieldState:
            textFieldZip.becomeFirstResponder()
        case textFieldZip:
            textFieldZip.resignFirstResponder()
        default:
            break
        }
        getLocationByAddress()
        return true
    }
}

extension DeviceDetailsViewController: DevicesActionSheetViewControllerDelegate {

    func deviceSelected(selectedDevice:Device) {
        
        switch selectedDevice {
        case .DIP_10:
            vmGate.MHz_310 = true
            vmGate.MHz_300 = false
        case .DIP_8:
            vmGate.MHz_310 = false
            vmGate.MHz_300 = true
        }
        setupController()
    }
}

extension DeviceDetailsViewController:AddressMapViewControllerDelegate {
    
    func newLocation(_ vmLocation:VMLocation) {
        
        vmGate.location = vmLocation
        setupGateLocation()
    }
}
