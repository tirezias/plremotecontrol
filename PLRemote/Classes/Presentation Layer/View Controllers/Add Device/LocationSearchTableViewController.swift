//
//  LocationSearchTableViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 7/21/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import MapKit

class LocationSearchTableViewController: BaseTableViewController {

    var items:[VMLocationSearch] = []
    var mapView: MKMapView? = nil
    var dataSource:TableViewDataSource!
    var handleMapSearchDelegate:HandleMapSearch? = nil
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        setupTableView()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedItem = dataSource.items[indexPath.row] as! VMLocationSearch
        handleMapSearchDelegate?.dropPinZoomIn(placemark: selectedItem.placeMark)
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Private methods
    private func setupTableView() {
        
        let configureCell:(TableViewCellBlock) = {cell, item, edited in
            cell.configureCell(item: item)
        }
        dataSource = TableViewDataSource([], aCellIdentifier: CellIdentifierLocation, aConfigureCellBlock: configureCell)
        tableView.dataSource = dataSource
        tableView.delegate = self
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        guard let mapView = mapView, let searchBarText = searchController.searchBar.text else { return }
        
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBarText
        request.region = mapView.region
        let search = MKLocalSearch(request: request)
        search.start { response, _ in
            
            guard let response = response else {
            
                return
            }
            var arrayVM:[VMLocationSearch] = [VMLocationSearch]()
            arrayVM = response.mapItems.map { (item) in
             
                VMLocationSearch(placemark: item.placemark)
            }
            self.dataSource.items = arrayVM
            self.tableView.reloadData()
        }
    }
}

extension LocationSearchTableViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        updateSearchResultsForSearchController(searchController: searchController)
    }
}
