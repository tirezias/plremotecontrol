//
//  AddressMapViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 7/3/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import MapKit

protocol AddressMapViewControllerDelegate: class {
    
    func newLocation(_ vmLocation:VMLocation)
}

protocol HandleMapSearch {
    
    func dropPinZoomIn(placemark:MKPlacemark)
}

class AddressMapViewController: BaseViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var buttonCurrentLocation: UIButton!
    @IBOutlet weak var labelSelectedLocationAddress: UILabel!
    @IBOutlet weak var rightBarButtonItem: UIBarButtonItem!
    
    var resultSearchController:UISearchController? = nil
    var selectedPin:MKPlacemark? = nil
    var searchIsActive:Bool = false
    
    weak var delegate:AddressMapViewControllerDelegate?
    var vmLocation:VMLocation?
    var searchBar:CustomSearchBar!
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupController()
    }

    
    // MARK: - Private methods
    private func setupController() {
        
        onClickCurrentLocation(self)
        mapView.showsUserLocation = false
        
        if(LocationManager.sharedInstance.latestLocation != nil) {
         
            let center:CLLocationCoordinate2D = LocationManager.sharedInstance.latestLocation!.coordinate
            let placeMark = MKPlacemark(coordinate: center)
            dropPinZoomIn(placemark: placeMark)
            getAddressFromLatLon(center: center)
        }
        
        // LOCATION SEARCH
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let locationSearchTable = storyboard.instantiateViewController(withIdentifier: "LocationSearchTableViewController") as! LocationSearchTableViewController
        locationSearchTable.mapView = mapView
        resultSearchController = CustomSearchController(searchResultsController: locationSearchTable)
        resultSearchController!.searchResultsUpdater = locationSearchTable
        
        searchBar = resultSearchController!.searchBar as? CustomSearchBar
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.showsCancelButton = false
        searchBar.placeholder = "Search for places"
        navigationItem.titleView = resultSearchController?.searchBar
        
        resultSearchController!.hidesNavigationBarDuringPresentation = false
        resultSearchController!.obscuresBackgroundDuringPresentation = true
        definesPresentationContext = true
        
        locationSearchTable.handleMapSearchDelegate = self
    }
    
    private func setupAddresLabel() {
        
        if(vmLocation != nil) {
            
            if(!vmLocation!.addressFull.isEmpty) {
                
                searchBar.text = vmLocation!.addressFull
            }
        }
        else {
            
        }
    }
    
    private func zoomAndCenterWithMiles(on centerCoordinate: CLLocationCoordinate2D, miles: Double) {
        
        let region = MKCoordinateRegion(center: centerCoordinate,
                                        latitudinalMeters: CLLocationDistance(exactly: miles)!,
                                        longitudinalMeters: CLLocationDistance(exactly: miles)!)
        mapView.setCenter(centerCoordinate, animated: true)
        mapView.setRegion(mapView.regionThatFits(region), animated: true)
    }
    
    private func getAddressFromLatLon(center:CLLocationCoordinate2D) {
        
        let geocoder: CLGeocoder = CLGeocoder()
        let location: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        geocoder.reverseGeocodeLocation(location, completionHandler: {[weak self] (placemarks, error) in
                
                if(placemarks != nil) {
                    
                    let pm = placemarks! as [CLPlacemark]
                    if (pm.count > 0) {
                        
                        let pm = placemarks![0]
                        self?.vmLocation = Utility.getAddressFrom(pm, center: center)
                        self?.setupAddresLabel()
                    }
                }
                else if (error != nil) {
                    
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
        })
    }
    
    
    // MARK: - Action methods
    @IBAction func onClickCurrentLocation(_ sender: Any) {
    
        if(LocationManager.sharedInstance.latestLocation != nil) {
                
            let center:CLLocationCoordinate2D = LocationManager.sharedInstance.latestLocation!.coordinate
            mapView.setCenter(center, animated: true)
            zoomAndCenterWithMiles(on: center, miles: 250)
            getAddressFromLatLon(center: center)
            let placeMark = MKPlacemark(coordinate: center)
            dropPinZoomIn(placemark: placeMark)
        }
    }
    
    @IBAction func onClickDone(_ sender: Any) {
        
        if(searchIsActive) {
            
            dismiss(animated: true, completion: nil)
        }
        else {
         
            if(vmLocation != nil) {
                
                delegate?.newLocation(vmLocation!)
                navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension AddressMapViewController: HandleMapSearch {
    
    func dropPinZoomIn(placemark:MKPlacemark){
        
        vmLocation = Utility.getAddressFrom(placemark, center: placemark.coordinate)
        // cache the pin
        selectedPin = placemark
        // clear existing pins
        mapView.removeAnnotations(mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        mapView.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: placemark.coordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
}

extension AddressMapViewController: UISearchBarDelegate {

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchIsActive = true
        rightBarButtonItem.title = "Cancel"
        rightBarButtonItem.setTitlePositionAdjustment(UIOffset(horizontal: 10, vertical: 0), for: .default)
        searchBar.setShowsCancelButton(false, animated: false)
        searchBar.text = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        searchIsActive = false
        rightBarButtonItem.title = "Done"
        rightBarButtonItem.setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: 0), for: .default)
        if(vmLocation != nil) {
         
            searchBar.text = vmLocation!.addressFull
        }
        searchBar.sizeToFit()
    }
    
    func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) {
        
        searchBar.setShowsCancelButton(false, animated: false)
    }
}
