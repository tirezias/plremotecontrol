//
//  DevicesActionSheetViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/25/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

protocol DevicesActionSheetViewControllerDelegate:class {
    
    func deviceSelected(selectedDevice:Device)
}

class DevicesActionSheetViewController: BaseViewController {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var containerView_10_Dip: UIView!
    @IBOutlet weak var containerView_8_Dip: UIView!
    @IBOutlet weak var imageView_10_Dip: UIImageView!
    @IBOutlet weak var imageView_8_Dip: UIImageView!
    @IBOutlet weak var constraintContainerBottom:NSLayoutConstraint!
    
    weak var delegate:DevicesActionSheetViewControllerDelegate?
    var selectedDevice:Device = .DIP_10
    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupSelectedDevice()
        setupController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        showActionSheetContainerWithAnimation()
    }
    
    
    // MARK: - Private methods
    private func setupSelectedDevice() {
        
        switch selectedDevice {
        case .DIP_10:
            containerView_10_Dip.backgroundColor = UIColor(named: "App_Background")
            imageView_10_Dip.image = UIImage(named: "radioButtonSelected")
            
            containerView_8_Dip.backgroundColor = .white
            imageView_8_Dip.image = UIImage(named: "radioButton")
        case .DIP_8:
            containerView_10_Dip.backgroundColor = .white
            imageView_10_Dip.image = UIImage(named: "radioButton")
            
            containerView_8_Dip.backgroundColor = UIColor(named: "App_Background")
            imageView_8_Dip.image = UIImage(named: "radioButtonSelected")
        }
    }
    
    private func setupController() {
        
        view.isOpaque = false
        view.backgroundColor = .clear
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.tapOnBGView))
        bgView?.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func tapOnBGView() {
        
        dismiss(animated: false, completion: nil)
    }
    
    private func showActionSheetContainerWithAnimation() {
        
        constraintContainerBottom.constant = 5
        UIView.animate(withDuration: 0.3) {
            
            self.view.layoutIfNeeded()
        }
    }
    
    private func hideActionSheetContainerWithAnimation() {
        
        constraintContainerBottom.constant = -305
        UIView.animate(withDuration: 0.3) {
            
            self.view.layoutIfNeeded()
        }
    }
    
    
    // MARK: - Action methods
    @IBAction func onClickDip_10(_ sender: Any) {
     
        selectedDevice = .DIP_10
        setupSelectedDevice()
        delegate?.deviceSelected(selectedDevice: selectedDevice)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onClickDip_8(_ sender: Any) {
        
        selectedDevice = .DIP_8
        setupSelectedDevice()
        delegate?.deviceSelected(selectedDevice: selectedDevice)
        dismiss(animated: false, completion: nil)
    }
}
