//
//  CustomSearchBar.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 7/25/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class CustomSearchBar: UISearchBar {

    override func setShowsCancelButton(_ showsCancelButton: Bool, animated: Bool) {
      
        super.setShowsCancelButton(false, animated: false)
    }
}

class CustomSearchController: UISearchController {
    
    lazy var _searchBar: CustomSearchBar = {
        
        [unowned self] in
        let customSearchBar = CustomSearchBar(frame: CGRect.zero)
        return customSearchBar
        }()
    
    override var searchBar: UISearchBar {
        
        get {
            
            return _searchBar
        }
    }
}
