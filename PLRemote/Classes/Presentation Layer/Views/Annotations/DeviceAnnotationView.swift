//
//  DeviceAnnotationView.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/23/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import MapKit

class DeviceAnnotationView: MKMarkerAnnotationView {

    internal override var annotation: MKAnnotation? { willSet { newValue.flatMap(configure(with:)) } }
    
    func configure(with annotation: MKAnnotation) {
        
        guard annotation is VMGate else { fatalError("Unexpected annotation type: \(annotation)") }
        markerTintColor = .clear
        glyphText = ""
        image = UIImage(named: "iconAnnotation")
    }
}
