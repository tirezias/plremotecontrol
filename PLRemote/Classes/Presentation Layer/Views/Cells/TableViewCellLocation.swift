//
//  TableViewCellLocation.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 7/21/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import MapKit

var CellIdentifierLocation:String = "CellIdentifierLocation"

class TableViewCellLocation: BaseTableViewCell {

    @IBOutlet weak var labelLocationName: UILabel!
    @IBOutlet weak var labelLocationAddress: UILabel!
    
    var viewModel:VMLocationSearch!
    
    override func configureCell(item: BaseViewModel)  {
        
        viewModel = item as? VMLocationSearch
        
        labelLocationName.text = viewModel.placeMark.name
        labelLocationAddress.text = parseAddress(selectedItem: viewModel.placeMark)
    }
    
    func parseAddress(selectedItem:MKPlacemark) -> String {
        
        // put a space between "4" and "Melrose Place"
        let firstSpace = (selectedItem.subThoroughfare != nil && selectedItem.thoroughfare != nil) ? " " : ""
        // put a comma between street and city/state
        let comma = (selectedItem.subThoroughfare != nil || selectedItem.thoroughfare != nil) && (selectedItem.subAdministrativeArea != nil || selectedItem.administrativeArea != nil) ? ", " : ""
        // put a space between "Washington" and "DC"
        let secondSpace = (selectedItem.subAdministrativeArea != nil && selectedItem.administrativeArea != nil) ? " " : ""
        let addressLine = String(
            format:"%@%@%@%@%@%@%@",
            // street number
            selectedItem.subThoroughfare ?? "",
            firstSpace,
            // street name
            selectedItem.thoroughfare ?? "",
            comma,
            // city
            selectedItem.locality ?? "",
            secondSpace,
            // state
            selectedItem.administrativeArea ?? ""
        )
        return addressLine
    }
}
