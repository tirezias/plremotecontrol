//
//  TableViewCellAddDevice.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 7/14/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

var CellIdentifierAddDevice:String = "CellIdentifierAddDevice"

class TableViewCellAddDevice: BaseTableViewCell {

    @IBOutlet weak var viewContainerCell: UIView!
    @IBOutlet weak var topView: UIView!
    
    var viewModel:VMAddNewDevice!
    
    override func awakeFromNib() {
        
        viewContainerCell.setShadow()
    }
    
    override func configureCell(item: BaseViewModel)  {
        
        viewModel = item as? VMAddNewDevice
        
        topView.isHidden = !viewModel.isExistDevice
    }
}
