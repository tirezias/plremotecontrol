//
//  PultCollectionViewCell.swift
//  PLRemote
//
//  Created by Ishkhan Gevorgyan on 8/31/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreLocation

protocol PultCollectionViewCellDelegate : NSObjectProtocol {
    
    func onLongPressCell(vmGate:VMGate)
    func onLongPressCellEnded(vmGate:VMGate)
    func onClickSettings(vmGate:VMGate)
}

class PultCollectionViewCell: BaseCollectionViewCell {
    
    var isAnimate: Bool! = true
    var pultImageView:UIImageView = UIImageView()
    var emptyPultImageView:UIImageView = UIImageView()
    var emptyView:UIView = UIView()
    var lineView:UIView = UIView()
    var nameLabel:UILabel = UILabel()
    var addressLabel:UILabel = UILabel()
    var locationImageView:UIImageView = UIImageView()
    var locationLabel:UILabel = UILabel()
    var hintView:UIView = UIView()
  //  var touchAndHold:UIImageView = UIImageView()
    var settingsButton = UIButton()
    var vmGate:VMGate? {
        
        didSet {
            
            self.startUpdatingLocation()
        }
    }
    weak var delegate:PultCollectionViewCellDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        pultImageView.frame = CGRect(x: 40, y: 124, width: self.frame.size.width - 80, height: self.frame.size.height - 144)
        pultImageView.contentMode = .scaleAspectFit
        
        emptyPultImageView.frame = CGRect(x: 40, y: 124, width: self.frame.size.width - 80, height: self.frame.size.height - 144)
        emptyPultImageView.contentMode = .scaleAspectFit
        
        emptyView.frame = CGRect(x: 50, y: 120, width: self.frame.size.width - 100, height: (self.frame.size.height - 160) / 2)
        emptyView.backgroundColor = .clear
        lineView.frame = CGRect(x: 0, y: 56, width: self.frame.size.width , height: 0.5)
        lineView.backgroundColor = .lightGray
        
        nameLabel.frame = CGRect(x: 20, y: 16 , width: self.frame.size.width - 100, height: 25)
        nameLabel.textAlignment = .left
        nameLabel.font = UIFont.boldSystemFont(ofSize: 21)
        nameLabel.numberOfLines = 1
        
        addressLabel.frame = CGRect(x: 50, y: 78 , width: self.frame.size.width - 130, height: 21)
        addressLabel.textAlignment = .left
        addressLabel.numberOfLines = 1
        
        locationImageView.frame = CGRect(x: 30, y: 78 , width: 23, height: 23)
        locationImageView.contentMode = .scaleAspectFit
        locationImageView.image = UIImage(named: "ic_location")
        locationLabel.frame = CGRect(x: addressLabel.frame.origin.x + addressLabel.frame.size.width, y: 78, width: 150, height: 15)
        locationLabel.textAlignment = .left
        locationLabel.textColor = .gray
        locationLabel.text = ""
        locationLabel.font = locationLabel.font.withSize(17)
              
        settingsButton.setImage(UIImage(named: "treeDots") , for: .normal)
        settingsButton.frame = CGRect(x: self.frame.size.width - 43, y: 13 , width: 35, height: 34)
        settingsButton.addTarget(self, action: #selector(settingsPressed), for: .touchUpInside)
        
        
        hintView.frame = CGRect(x: -100, y: -200 , width: self.frame.size.width + 200, height: self.frame.size.height + 400)
        hintView.backgroundColor = UIColor(hexString: "000000").withAlphaComponent(0.5)
        hintView.layer.cornerRadius = 5
        hintView.layer.masksToBounds = false
        hintView.isUserInteractionEnabled = false
        
//        touchAndHold.frame = CGRect(x: 0, y: 0 , width: 172, height: 50)
//        touchAndHold.center = CGPoint(x: pultImageView.center.x, y: pultImageView.center.y - self.frame.size.height / 3.6)
//        touchAndHold.image = UIImage(named: "touchAndHold")
//
//        touchAndHold.isHidden = true
        hintView.isHidden = true
        emptyPultImageView.isHidden = true
    
        self.addSubview(nameLabel)
        self.addSubview(addressLabel)
        self.addSubview(locationLabel)
        self.addSubview(locationImageView)
        self.addSubview(emptyView)
        self.addSubview(lineView)
        self.addSubview(settingsButton)
        self.addSubview(pultImageView)
        self.addSubview(hintView)
        self.addSubview(emptyPultImageView)
        
    //    self.addSubview(touchAndHold)

       if UserDefaults.standard.integer(forKey: "doWeSeeHint") < 1
       {
         let tempNumber = UserDefaults.standard.integer(forKey: "doWeSeeHint")
         UserDefaults.standard.set(tempNumber + 1, forKey: "doWeSeeHint")
               emptyPultImageView.isHidden = false
               hintView.isHidden = false
        }
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPress(longPressGestureRecognizer:)))
        self.emptyView.addGestureRecognizer(longPressRecognizer)
    }
    
    @objc
    func settingsPressed() {
        
         delegate?.onClickSettings(vmGate: vmGate!)
    }
    
    @objc
    func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if (longPressGestureRecognizer.state == .began) {
            
            if (vmGate?.MHz_300 == true) {
                
                pultImageView.image = UIImage(named: "plremote300selected")
            }
            else {
                
                pultImageView.image = UIImage(named: "plremote310selected")
            }
            emptyPultImageView.isHidden = true
            hintView.isHidden = true
            delegate?.onLongPressCell(vmGate: vmGate!)
        }
        if (longPressGestureRecognizer.state == .ended) {
            
            if (vmGate?.MHz_300 == true) {
                
                pultImageView.image = UIImage(named: "plremote300")
            }
            else {
                
                pultImageView.image = UIImage(named: "plremote310")
            }
            delegate?.onLongPressCellEnded(vmGate: vmGate!)
        }
    }
    
    func startAnimate() {
        
        let shakeAnimation = CABasicAnimation(keyPath: "transform.rotation")
        shakeAnimation.duration = 0.05
        shakeAnimation.repeatCount = 4
        shakeAnimation.autoreverses = true
        shakeAnimation.duration = 0.2
        shakeAnimation.repeatCount = 99999
        
        let startAngle: Float = (-0.5) * 3.14159/180
        let stopAngle = -startAngle
        
        shakeAnimation.fromValue = NSNumber(value: startAngle as Float)
        shakeAnimation.toValue = NSNumber(value: 3 * stopAngle as Float)
        shakeAnimation.autoreverses = true
        shakeAnimation.timeOffset = 290 * drand48()
        
        let layer: CALayer = self.layer
        layer.add(shakeAnimation, forKey:"animate")
        isAnimate = true
    }
    
    func stopAnimate() {
        
        let layer: CALayer = self.layer
        layer.removeAnimation(forKey: "animate")
        isAnimate = false
    }
    
    func startUpdatingLocation() {
        
        if (LocationManager.sharedInstance.latestLocation != nil && vmGate != nil) {
            
            LocationManager.sharedInstance.registerForHeading { [weak self] (value) in
                
                if(self?.vmGate!.location != nil) {
                        
                    if (self?.vmGate!.location!.latitude != 0 && self?.vmGate!.location!.longitude != 0){
                        
                        let location = CLLocation(latitude: (self?.vmGate!.location!.latitude)!, longitude: (self?.vmGate!.location!.longitude)!)
                        let string = LocationManager.sharedInstance.latestLocation?.distance(from: location)
                        if var number = string {
                            
                            var result = ""
                            number = number / MYLE
                            result = number.roundTo(places: 1).cleanValue()+"Mi".localized
                            self?.locationLabel.text = "  "+result+"  "
                        }
                        let value2 = LocationManager.sharedInstance.getBearingBetweenTwoPoints1(point1: LocationManager.sharedInstance.latestLocation!,
                                                                                                point2: location)
                        if self?.locationImageView != nil {
                            
                            self?.locationImageView.transform = CGAffineTransform.init(rotationAngle: CGFloat((value2 + (360 - value!)) * Double.pi / 180))
                        }
                    }
                }
            }
        }
    }
}
