//
//  TableViewCellMyDevices.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/26/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

//var CellIdentifierMyDevice: String = "CellIdentifierMyDevice"

class TableViewCellMyDevices: BaseTableViewCell {

    @IBOutlet weak var viewContainerCell: UIView!
    @IBOutlet weak var labelDeviceName: UILabel!
    @IBOutlet weak var constraitWidthNotConnected:NSLayoutConstraint!
    
    var viewModel:VMGate!
    
    override func awakeFromNib() {
        
        viewContainerCell.layer.masksToBounds = false
        viewContainerCell.layer.shadowColor = UIColor.blue.cgColor
        viewContainerCell.layer.shadowOpacity = 0.1
        viewContainerCell.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewContainerCell.layer.shadowRadius = 5
    }
    
    override func configureCell(item: BaseViewModel)  {
        
        viewModel = item as? VMGate
        
        labelDeviceName.text = viewModel.name
    }
}
