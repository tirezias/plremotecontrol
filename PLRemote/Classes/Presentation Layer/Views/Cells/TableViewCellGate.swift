//
//  TableViewCellGate.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/23/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreLocation

var CellIdentifierGate: String = "CellIdentifierGate"

class TableViewCellGate: BaseTableViewCell {

    @IBOutlet weak var viewContainerCell: UIView!
    @IBOutlet weak var imageViewGate: UIImageView!
    @IBOutlet weak var labelDeviceName: UILabel!
    @IBOutlet weak var labelDeviceLocation: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    
    var viewModel:VMGate!
    
    override func awakeFromNib() {
        
        viewContainerCell.setShadow()
    }
    
    override func configureCell(item: BaseViewModel)  {
        
        viewModel = item as? VMGate
        
        imageViewGate.image = viewModel.MHz_300 ? UIImage(named: "plremote300") : UIImage(named: "plremote310")
        labelDeviceName.text = viewModel.name
        if(viewModel.location != nil) {
                
            labelDeviceLocation.text = viewModel.location!.street 
        }
        startUpdatingLocation()
    }
    
     func  startUpdatingLocation() {
            
            if (LocationManager.sharedInstance.latestLocation != nil && viewModel != nil) {
                
                LocationManager.sharedInstance.registerForHeading { [weak self] (value) in
                    
                    if(self?.viewModel.location != nil) {
                            
                        if (self?.viewModel!.location!.latitude != 0 && self?.viewModel!.location!.longitude != 0) {
                            
                            let location = CLLocation(latitude: (self?.viewModel!.location!.latitude)!, longitude: (self?.viewModel!.location!.longitude)!)
                            let string = LocationManager.sharedInstance.latestLocation?.distance(from: location)
                            if var number = string {
                                
                                var result = ""
                                number = number / MYLE
                                result = number.roundTo(places: 1).cleanValue()+"Mi".localized
                                self?.labelDistance.text = "  "+result+"  "
                        }
                    }
                }
            }
        }
    }
}
