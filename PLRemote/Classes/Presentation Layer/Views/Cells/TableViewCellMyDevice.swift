//
//  TableViewCellMyDevice.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/26/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

var CellIdentifierMyDevice:String = "CellIdentifierMyDevice"

class TableViewCellMyDevice: BaseTableViewCell {

    @IBOutlet weak var viewContainerCell: UIView!
    @IBOutlet weak var labelDeviceName: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var constraitWidthNotConnected:NSLayoutConstraint!
    
    var viewModel:VMDevice!
    
    override func awakeFromNib() {
        
        viewContainerCell.setShadow()
    }
    
    override func configureCell(item: BaseViewModel)  {
        
        viewModel = item as? VMDevice
        
        labelDeviceName.text = viewModel.deviceName
        constraitWidthNotConnected.constant = viewModel.isConnected ? 0 : 125
        topView.isHidden = viewModel.isFirstDevice
    }
}
