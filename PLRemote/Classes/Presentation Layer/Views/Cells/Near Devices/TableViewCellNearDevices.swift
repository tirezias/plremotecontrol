//
//  TableViewCellNearDevices.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/31/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

var CellIdentifierNearDevices: String = "CellIdentifierNearDevices"

class TableViewCellNearDevices: BaseTableViewCell {

    @IBOutlet weak var labelDeviceName: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var viewModel:VMDevice!
    
    override func configureCell(item: BaseViewModel)  {
        
        viewModel = item as? VMDevice
        
        labelDeviceName.text = viewModel.deviceName
        containerView.backgroundColor = viewModel.isSelected ? UIColor.rgb(243.0, green: 247.0, blue: 254.0) : .white
    }
}
