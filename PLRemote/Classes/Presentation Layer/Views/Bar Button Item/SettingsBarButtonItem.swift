//
//  SettingsBarButtonItem.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 6/24/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class SettingsBarButtonItem: UIBarButtonItem {

    static let shared = SettingsBarButtonItem()
    var imageViewWarning:UIImageView!
    
    required init?(coder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    override init() {
        
        super.init()
        
        let view:UIView = UIView.init(frame: CGRect(x:0, y:0, width:100, height:40))
        view.widthAnchor.constraint(equalToConstant: 100).isActive = true
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        // MARK: - BASKET
        // Adds settings image to view
        let image:UIImage = UIImage(named: "iconSettings")!
        let imageView:UIImageView = UIImageView(frame: CGRect(x:view.frame.size.width - image.size.width,
                                                                    y:7,
                                                                    width:image.size.width,
                                                                    height:image.size.height))
        imageView.image = image
        view.addSubview(imageView)
        
        imageViewWarning = UIImageView(frame: CGRect(x:imageView.frame.origin.x, y:imageView.frame.origin.y, width:14, height:14))
        imageViewWarning.image = UIImage(named: "iconWarning")
        view.addSubview(imageViewWarning)
        imageViewWarning.isHidden = BluetoothManager.shared.getConnectedDeviceStatus()
        
        // Adds action button to view
        let button:UIButton = UIButton(type: .custom)
        button.frame = view.frame
        button.addTarget(self, action: #selector(self.onCLickSettings), for: .touchUpInside)
        view.addSubview(button)
        
        customView = view
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.setupWarningImageView(notification:)),
                                               name: NSNotification.Name(rawValue: CONNECTION_STATE),
                                               object: nil)
    }
    
    @objc func setupWarningImageView(notification:Notification) {
        
        let dict = notification.userInfo
        let state:Bool = dict![CONNECTION_STATE] as! Bool
        imageViewWarning.isHidden = state
    }
    
    @objc func onCLickSettings() {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: OPEN_SETTINGS), object: nil)
    }
}
