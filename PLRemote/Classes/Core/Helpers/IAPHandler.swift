//
//  IAPHandler.swift
//  GermanSecrets
//
//  Created by Ishkhan Gevorgyan on 1/13/19.
//  Copyright © 2019 Ishkhan Gevorgyan. All rights reserved.
//


import UIKit
import StoreKit

enum IAPHandlerAlertType{
    
    case disabled
    case restored
    case purchased
    
    func message() -> String {
        
        switch self {
            
            case .disabled: return "Purchases are disabled in your device!"
            case .restored: return "You've successfully restored your purchase!"
            case .purchased: return "You've successfully bought this purchase!"
        }
    }
}

class IAPHandler: NSObject {
    
    static let shared = IAPHandler()
    
    fileprivate var productID = ""
    fileprivate var productsRequest = SKProductsRequest()
    fileprivate var iapProducts = [SKProduct]()
   // let servicePunkt = ServicePunkt()
    var purchaseStatusBlock: ((IAPHandlerAlertType) -> Void)?
    var tempIndex = -1
    
    // MARK: - MAKE PURCHASE OF A PRODUCT
    func canMakePurchases() -> Bool {
        
        return SKPaymentQueue.canMakePayments()
    }
    
    func purchaseMyProduct(index: Int) {
        
        if iapProducts.count == 0 { return }
        if self.canMakePurchases() {
            
            let product = iapProducts[index]
            tempIndex = index + 1
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            productID = product.productIdentifier
        }
        else {
            
            purchaseStatusBlock?(.disabled)
        }
    }
    
    // MARK: - RESTORE PURCHASE
    func restorePurchase() {
        
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    
    // MARK: - FETCH AVAILABLE IAP PRODUCTS
    func fetchAvailableProducts() {
        
        // Put here your IAP Products ID's
        let productIdentifiers : Set<String> = Set(arrayLiteral: "0packet", "1packet", "2packet")
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest.delegate = self
        productsRequest.start()
    }
}

extension IAPHandler: SKProductsRequestDelegate, SKPaymentTransactionObserver{
    // MARK: - REQUEST IAP PRODUCTS
    func productsRequest (_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        
        if (response.products.count > 0) {
            
            iapProducts = response.products
            for product in iapProducts {
                
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .currency
                numberFormatter.locale = product.priceLocale
             
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        purchaseStatusBlock?(.restored)
    }
    
    // MARK:- IAP PAYMENT QUEUE
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction:AnyObject in transactions {
          
            if let trans = transaction as? SKPaymentTransaction {
                
                switch trans.transactionState {
                case .purchased:
                    
                    print("purchased")
                    UserDefaults.standard.set(tempIndex, forKey: PURCHESED_INDEX)
                    UserDefaults.standard.synchronize()
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.purchaseStatusBlock?(.purchased)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: PLAN_UPGRADE), object: nil, userInfo: nil)
                case .failed:
                    
                    print("failed")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    purchaseStatusBlock?(.disabled)
                case .restored:
                    
                    print("restored")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    purchaseStatusBlock?(.restored)
                default:
                    
                    break
                }
            }
        }
    }
 
    static func receiptValidation() {
       
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString as AnyObject, "password" : iTunesKey as AnyObject]

        do {
            let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            let storeURL = URL(string: verifyReceiptURLLive)!
            var storeRequest = URLRequest(url: storeURL)
            storeRequest.httpMethod = "POST"
            storeRequest.httpBody = requestData

            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: storeRequest, completionHandler: {  (data, response, error) in

                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data ?? Data(), options: JSONSerialization.ReadingOptions.mutableContainers)

                    if let statusCode:Int = (jsonResponse as! NSDictionary)["status"] as? Int {
                        
                        if (statusCode == 0) {
                            
                           if let receipt = (jsonResponse as! NSDictionary)["receipt"] as? [String:Any] {
                            
                                print(receipt)
                                if (receipt["bundle_id"]! as!String == Bundle.main.bundleIdentifier) {
                                    
                                    if let date = getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                                        
                                        let nowDate = Date()
                                        print("hello",nowDate,date)
                                        if (nowDate > date) {
                                            
                                            UserDefaults.standard.set(-1, forKey: PURCHESED_INDEX)
                                            UserDefaults.standard.synchronize()
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            
                            receiptValidationSandbox()
                        }
                    }
                }
                catch let parseError {
                    
                    print(parseError)
                }
            })
            task.resume()
        }
        catch let parseError {
            
            print(parseError)
        }
    }

   static func receiptValidationSandbox() {

    if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL, FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {

        do {
            let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
            print(receiptData)
            let receiptString = receiptData.base64EncodedString(options: [])
            let jsonDict: [String: AnyObject] = ["receipt-data" : receiptString as AnyObject, "password" : iTunesKey as AnyObject]

            do {
                let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                print(requestData)
                let storeURL = URL(string: verifyReceiptURL)!
                var storeRequest = URLRequest(url: storeURL)
                storeRequest.httpMethod = "POST"
                storeRequest.httpBody = requestData

                let session = URLSession(configuration: URLSessionConfiguration.default)
                let task = session.dataTask(with: storeRequest, completionHandler: {  (data, response, error) in

                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        print("=======>",jsonResponse)
                        if let statusCode:Int = (jsonResponse as! NSDictionary)["status"] as? Int {
                            
                            if (statusCode == 0) {
                                
                                if let receipt = (jsonResponse as! NSDictionary)["receipt"] as? [String:Any] {
                                    
                                    print(receipt)
                                    if receipt["bundle_id"]! as!String == Bundle.main.bundleIdentifier {
                                        
                                        if let date = getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                                            
                                            let nowDate = Date()
                                            print("hello",nowDate,date)
                                            if (nowDate > date) {
                                                
                                                UserDefaults.standard.set(-1, forKey: PURCHESED_INDEX)
                                                UserDefaults.standard.synchronize()
                                            }
                                        }
                                    }
                                }
                            }
                            else {

                            }
                        }
                    }
                    catch let parseError {
                        
                        print(parseError)
                    }
                })
                task.resume()
            } catch let parseError {
                
                print(parseError)
            }
        }
        catch {
            
            print("Couldn't read receipt data with error: " + error.localizedDescription) }
        }
    }
    
   static func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            if let expiresDate = lastReceipt["expires_date"] as? String {
              
                return formatter.date(from: expiresDate)
            }
            return nil
        }
        else {
            
            return nil
        }
    }
}


