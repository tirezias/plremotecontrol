//
//  TransactionExtension.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func pushControllerWithStoryboard(_ storyboard:String, storyboardId:String) {
        
        let storyboard = UIStoryboard.init(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: storyboardId)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func openTabBarController() {
        
        let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MainTabBarController") as! UITabBarController
        let serviceDevice = ServiceDevice()
        if (serviceDevice.getGatesCount() == 0) {
            
            viewController.selectedIndex = 1
            viewController.tabBar.isHidden = true
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = viewController
    }
    
    func openTabBarControllerFirstTime() {
        
        UserDefaults.standard.set(true, forKey: HELP_ALREADY_OPENED)
        UserDefaults.standard.synchronize()
        let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "MainTabBarController") as! UITabBarController
        viewController.selectedIndex = 1
        viewController.tabBar.isHidden = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = viewController
    }
}
