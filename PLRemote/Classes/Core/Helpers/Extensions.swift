//
//  Extensions.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//


import UIKit
import ImageIO
import MapKit
import NVActivityIndicatorView

class Extensions: NSObject {

}

@IBDesignable class TIFAttributedLabel: UILabel {

    @IBInspectable var fontSize: CGFloat = 13.0

    @IBInspectable var fontFamily: String = "Nunito-Regular"

    override func awakeFromNib() {
        
        let attrString = NSMutableAttributedString(attributedString: self.attributedText!)
        attrString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: self.fontFamily, size: self.fontSize)!, range: NSMakeRange(0, attrString.length))
        self.attributedText = attrString
    }
}

// MARK: - UIView
extension UIView {
    
    // MARK: - UIView - layer
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: layer.shadowColor!)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    func setShadow() {
        
        layer.masksToBounds = false
        layer.shadowColor = UIColor.blue.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 5
    }
}

// MARK: - String
extension String {
    
    var localized: String {
        
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}


// MARK: - UIColor
extension UIColor {
    
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    convenience init(hexString: String) {
        
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UIViewController:NVActivityIndicatorViewable {
    
    func showActivityIndicator() {
        
        let backgrounColor:UIColor = UIColor.gray
        startAnimating(CGSize(width: 50, height: 50),
                       message: nil,
                       messageFont: nil,
                       type: .ballRotateChase,
                       color: UIColor.white,
                       padding: nil,
                       displayTimeThreshold: nil,
                       minimumDisplayTime: nil,
                       backgroundColor: backgrounColor.withAlphaComponent(0.2),
                       textColor: nil)
    }
    
    @objc func hideActivityIndicator() {
        
        stopAnimating()
    }
    
    func showSuccessIndicatorWithMessage(_ message:String) {
        
        startAnimating(CGSize(width: 0, height: 0),
                       message: message,
                       messageFont: UIFont(name: "AvenirNext-Medium", size: 17),
                       type: .blank,
                       color: UIColor.rgb(228, green: 32, blue: 113),
                       padding: nil,
                       displayTimeThreshold: nil,
                       minimumDisplayTime: nil,
                       backgroundColor: nil,
                       textColor: UIColor.white)
        
        perform(#selector(self.hideActivityIndicator), with: nil, afterDelay: 1.0)
    }
    
    func showAlertWithMessage(message: String, title:String) {
        
        DispatchQueue.main.async {
            
            let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let actionButton: UIAlertAction = UIAlertAction(title: "Ok", style: .default) { action -> Void in}
            alertController.addAction(actionButton)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showErrorWithErrorModel(_ error:ErrorDataModel?) {
        
        if(error != nil) {
            
        }
    }
}

extension Float {
    
    var clean: String {
        
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        
        let divisor = pow(10.0, Double(places))
        let returnValue = (self * divisor).rounded()
        if (returnValue <= 0.1) {
            
            return 0.1
        }
        return (self * divisor).rounded() / divisor
    }
    
    func cleanValue() -> String {
        
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}


extension UITableView {
    
    func setEmptyView(title: String?, message: String?) {
        
        if(title != nil || message != nil) {
            
            let emptyView = UIView(frame: CGRect(x: self.center.x,
                                                 y: self.center.y,
                                                 width: self.bounds.size.width,
                                                 height: self.bounds.size.height))
            let titleLabel = UILabel()
            let messageLabel = UILabel()
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
            messageLabel.translatesAutoresizingMaskIntoConstraints = false
            titleLabel.textColor = UIColor.black
            titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
            messageLabel.textColor = UIColor.lightGray
            messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
            emptyView.addSubview(titleLabel)
            emptyView.addSubview(messageLabel)
            titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
            titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
            messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
            messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
            titleLabel.text = title
            messageLabel.text = message
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            
            self.backgroundView = emptyView
            self.separatorStyle = .none
        }
        else {
         
            self.backgroundView = nil
            self.separatorStyle = .none
        }
    }
    
    func restore() {
        
        self.backgroundView = nil
        self.separatorStyle = .none
    }
}
