/*
BLE class is the main class working with remote controller.
1. At first it is necessary to inherit the BLEDelegate protocol and overload all the inherited functions (see at the end of this file).
2. Then create an object of the BLE class ( initialize the object with init() function by passing the delegate created at the previous step).
3. When it's necessary to find the remote controller one must call startScan() function on BLE object..
   The BLEDelegate OnBluetoothState () callback gets invoked when the central manager’s state is updated. The function argument state is true when
   Bluetooth is turned on, false otherwise..
   The BLEDelegate OnScanResult() callback gets invoked when a new device is found.
   The scan can be stopped with stopScan() function or by connecting to one of the found devices.
4. To connect to remote controller one needs to call the connect() function which accepts as an argument the CBPeripheral object received at OnScanResult() call.
5. The connect() function can also be called before stopping the scan. In that case the scan will stop automatically.
   In particular, the connect() can be called inside OnScanResult() callback.
6. The BLEDelegate OnConnectionState() function is called when the device connection state is changed. The function argument is true when the remote controller is connected,
   false otherwise. Also you can get the connection state by calling the getConnectionState() function.
7. The remote controller can be disconnected by calling the disconnect() function.
8. If the button of the remote controller is pushed and it is connected, the BLEDelegate OnButtonPressed() callback gets invoked.
9. To open a door with the remote controller (it shall be connected to phone) it's necessary to call startTransmit() function on BLE object with following arguments - data: Int16,
   frequency: Frequency,
   dipSwitchCount: Int32 arguments;
 
   data - is the code set by dip switches. For example if the switch count is 10 the data value is an integer in 0 to 1023 range,
   frequency - BLE.Frequency enum type (300mhz - F300, 310mhz - F310),
   dipSwitchCount - dip switches count (8 or 10).
 
   After calling the function the remote controller is constantly Transmitting until stopTransmit() function is called. If one needs to send the garage code to the remote controller without transmitting it to door (the start and the
   end of the transmission is determined by keeping pushed the remote controller button), he must call the setDataForRemoteButton() function. The arguments are the same as for
   the startTransmit() function.
 */

import Foundation
import CoreBluetooth

class BLE: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {    
    
    weak open var mDelegate: BLEDelegate? // The delegate object that will receive BLE events
    private let mServiceID = CBUUID(string: "00001523-1212-efde-1523-785feabcd123")
    private let mTxCharacteristic = CBUUID(string: "00001525-1212-efde-1523-785feabcd123")
    private let mRxCharacteristic = CBUUID(string: "00001524-1212-efde-1523-785feabcd123")
    private var mCurrentCharacteristic: CBCharacteristic! = nil
    var mCentralManager: CBCentralManager!
    private var mPeripheral: CBPeripheral! = nil
    private var mConnected: Bool = false
    private var mCurrentData: Int16 = 0
    private var mCurrentFrequency: Frequency = Frequency.F300
    private var mCurrentDipSwitchCount: Int32 = 0
    private var mIsStartTransmit: Bool = false
    private var mIsSetDataForRemoteButton = false
    
    static let shared = BLE()
    
    enum Frequency {
        case F300, F310
    }
    
    func startCentralManager() {
     
        mCentralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    // Invoked when the central manager’s state is updated. This is where we kick off the scan if Bluetooth is turned on.
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
          case .poweredOn:
            mDelegate?.OnBluetoothState(self, state: true)
          case .poweredOff, .unknown, .unsupported, .resetting, .unauthorized:
            mDelegate?.OnBluetoothState(self, state: false)
        @unknown default:
            fatalError()
        }
    }
        
    // BLE peripheral discovered
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
       
        if mConnected == false {
            
            mDelegate?.OnScanResult(self, peripheral: peripheral, deviceName: peripheral.name!)
        }
    }
    
    // Connected BLE peripheral
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        mPeripheral.delegate = self
        mPeripheral.discoverServices(nil)
        mConnected = true
        stopScan()
        mDelegate?.OnConnectionState(self, state: true)
    }
    
    // Disconnected BLE peripheral
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        if mPeripheral != nil {
            
            mPeripheral.delegate = nil
            mPeripheral = nil
        }
        
        startCentralManager()
        mConnected = false
        mDelegate?.OnConnectionState(self, state: false)       
    }
    
    // Invoked when you discover the peripheral’s available services
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        for service in peripheral.services! {
            
            if service.uuid == mServiceID {
                
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    // Invoked when you discover the characteristics of a specified service
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
       
        for characteristic in service.characteristics! {
            
            let tempCharacteristic = characteristic as CBCharacteristic
            
            if tempCharacteristic.uuid == mRxCharacteristic {
                peripheral.setNotifyValue(true, for: tempCharacteristic)
            }
            else if tempCharacteristic.uuid == mTxCharacteristic {
                mCurrentCharacteristic = tempCharacteristic
            }
        }
    }
    
    // Invoked when you write data to a characteristic descriptor’s value
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        
            guard error == nil else {
            return
        }
    }
    
    // Invoked when you retrieve a specified characteristic’s value, or when the peripheral device notifies your app that the characteristic’s value has changed
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        mDelegate?.OnButtonPressed(self)
    }
    
    public func startScan() {
        
        if(mCentralManager != nil) {
         
            mCentralManager.scanForPeripherals(withServices: [self.mServiceID], options: .none)
        }
    }
    
    public func stopScan() {
        
        if(mCentralManager != nil) {
            
            mCentralManager.stopScan()
        }
    }
    
    public func getConnectionState() -> Bool {
        
        return mConnected
    }
    
    // Connecting BLE peripharal
    public func connect(peripheral: CBPeripheral) {
        
        mPeripheral = peripheral
        mCentralManager.connect(mPeripheral, options: [CBConnectPeripheralOptionNotifyOnDisconnectionKey : true])
    }
    
    // Disconnecting BLE peripheral
    public func disconnect() {
     
        if(mPeripheral != nil) {
                
            mCentralManager.cancelPeripheralConnection(mPeripheral)
        }
    }
    
    public func startTransmit(data: Int16, frequency: Frequency, dipSwitchCount: Int32) -> Bool {
       
        if mConnected == false {
            
            return false
        }
        
        mIsStartTransmit = true
        mCurrentData = data
        mCurrentFrequency = frequency
        mCurrentDipSwitchCount = dipSwitchCount
        
        return writeData(data: data, frequency: frequency, dipSwitchCount: dipSwitchCount)
    }
    
    public func endTransmit() -> Bool {
      
        if mIsStartTransmit == false {
            return false
        }
        
        mIsStartTransmit = false
        
        return writeData(data: mCurrentData, frequency: mCurrentFrequency, dipSwitchCount: mCurrentDipSwitchCount)
    }
    
    public func setDataForRemoteButton(data: Int16, frequency: Frequency, dipSwitchCount: Int32) -> Bool {
        
        if mIsStartTransmit == true {
            return false
        }
        
        mIsSetDataForRemoteButton = true
        let result: Bool = writeData(data: data, frequency: frequency, dipSwitchCount: dipSwitchCount)
        mIsSetDataForRemoteButton = false
        return result
    }
    
    private func writeData(data: Int16, frequency: Frequency, dipSwitchCount: Int32) -> Bool {
      
        var buffer: [UInt8] = [0,0,0,0,0,0,0,0,0,0,0,0,0]
          print("binar",data)
        if data > 1023 || (frequency != Frequency.F300 && frequency != Frequency.F310) || (dipSwitchCount != 8 && dipSwitchCount != 10) {
            return false
        }
        
        for i in 0 ..< 13 {
            if i < dipSwitchCount {
                let switchBit = data & (1 << i)
                
                if switchBit != 0 {
                    buffer[i] = 49 // '1'
                }
                else {
                    buffer[i] = 48 // '0'
                }
            }
        }
        
        if frequency == Frequency.F300 {
            if dipSwitchCount == 8 {
                buffer[8] = 48 // '0'
            }
            else {
                buffer[10] = 48 // '0'
            }
        }
        else {
            if dipSwitchCount == 8 {
                buffer[8] = 49 // '1'
            }
            else {
                buffer[10] = 49 // '1'
            }
        }
        
        if mIsSetDataForRemoteButton {
            if dipSwitchCount == 8 {
                buffer[9] = 0
            }
            else {
                buffer[11] = 0
            }
        }
        else {
            if mIsStartTransmit == true {
                if dipSwitchCount == 8 {
                    buffer[9] = 83 // 'S'
                }
                else {
                    buffer[11] = 83 // 'S'
                }
            }
            else {
                if dipSwitchCount == 8 {
                    buffer[9] = 69 // 'E'
                }
                else {
                    buffer[11] = 69 // 'E'
                }
            }
        }
        
        let result = String(bytes: buffer, encoding: String.Encoding.ascii)
        let valueString = (result! as NSString).data(using: String.Encoding.utf8.rawValue)
        
        if mPeripheral != nil {
            if let txCharacteristic = mCurrentCharacteristic {
                mPeripheral.writeValue(valueString!, for: txCharacteristic, type: CBCharacteristicWriteType.withResponse)
            }
        }
        
        return true
    }
}

protocol BLEDelegate : NSObjectProtocol {
    
    func OnScanResult(_ importer: BLE, peripheral: CBPeripheral, deviceName: String)
    
    func OnConnectionState(_ importer: BLE, state: Bool)
    
    func OnBluetoothState(_ importer: BLE, state: Bool)
    
    func OnButtonPressed(_ importer: BLE)
}
