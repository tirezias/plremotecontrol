//
//  Utility.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import MapKit

struct TMFieldValidation {
    
    var success:Bool = true
    var errorMessage:String?
}

class Utility: NSObject {
    
    static func getAddressFrom(_ placeMark:CLPlacemark, center:CLLocationCoordinate2D) -> VMLocation? {
        
        let vmLocation:VMLocation = VMLocation()
        vmLocation.latitude = center.latitude
        vmLocation.longitude = center.longitude
        if (placeMark.name != nil) {
            
            vmLocation.addressFull = vmLocation.addressFull + placeMark.name! + ", "
            vmLocation.street = placeMark.name!
        }
        if (placeMark.locality != nil) {
            
            vmLocation.addressFull = vmLocation.addressFull + placeMark.locality! + ", "
            vmLocation.city = placeMark.locality!
        }
        if (placeMark.country != nil) {
            
            vmLocation.addressFull = vmLocation.addressFull + placeMark.country! + ", "
            vmLocation.country = placeMark.country!
        }
        if (placeMark.postalCode != nil) {
            
            vmLocation.zip = placeMark.postalCode!
        }
        if(!vmLocation.addressFull.isEmpty) {
                
            let index = vmLocation.addressFull.index(vmLocation.addressFull.startIndex, offsetBy: vmLocation.addressFull.count - 2)
            vmLocation.addressFull = String(vmLocation.addressFull[..<index])
            
            return vmLocation
        }
         
        return nil
    }
}

