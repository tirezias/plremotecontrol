//
//  DataSourceTableView.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

@objc protocol DataSourceTableViewDelegate {
    
    @objc optional func currentCellRowNumber(indexPath: IndexPath, cell:UITableViewCell)
    @objc optional func lazyLoadWith(skip: Int, limit:Int)
    @objc optional func editCellAtIndexPath(_ indexPath:IndexPath, editingStyle:UITableViewCell.EditingStyle)
}

typealias TableViewCellBlock = (_ cell : BaseTableViewCell, _ id:BaseViewModel, _ isEditing: Bool) -> ()

class DataSourceTableView: NSObject, UITableViewDataSource {

    var items: NSMutableArray = NSMutableArray()
    var cellIdentifier: String = ""
    var isEditing: Bool = false
    var configureCellBlock: TableViewCellBlock = {_,_,_ in }
    weak var delegate:DataSourceTableViewDelegate?
    
    var emptyViewMessage:String?
    var emptyViewTitle:String?
    
    init(_ anItems: NSArray, aCellIdentifier: String, aConfigureCellBlock: @escaping TableViewCellBlock)  {
        
        super.init()
        
        self.items = anItems.mutableCopy() as! NSMutableArray
        self.cellIdentifier = aCellIdentifier
        self.configureCellBlock = aConfigureCellBlock
        self.isEditing = false
    }
    
    func setItems(anItems: NSMutableArray) {
        
        items = anItems.mutableCopy() as! NSMutableArray
    }
    
    func itemAtIndexPath(indexPath: IndexPath) -> BaseViewModel {
        
        var item: BaseViewModel = BaseViewModel()
        item.cellIdentifier = self.cellIdentifier
        if (indexPath.row < items.count) {
            
            item = items.object(at: indexPath.row) as! BaseViewModel
        }
        
        return item
    }
    
    func removeItem(item: AnyObject) {
        
        if (items.contains(item)) {
            
            items.remove(item)
        }
    }
    
    func removeItemAtIndex(indexPath: NSIndexPath) {
        
        if (indexPath.row < items.count) {
            
            items.removeObject(at: indexPath.row)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(items.count == 0 && (emptyViewTitle != nil || emptyViewMessage != nil)) {
            
            tableView.setEmptyView(title: emptyViewTitle, message: emptyViewMessage)
        }
        else {
            
            tableView.restore()
        }
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item: BaseViewModel = itemAtIndexPath(indexPath: indexPath)
        if(!item.cellIdentifier.isEmpty) {
        
            cellIdentifier = item.cellIdentifier
        }
        
        let cell:BaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BaseTableViewCell
        configureCellBlock(cell, item, self.isEditing)
        if(delegate != nil) {
         
            delegate?.currentCellRowNumber!(indexPath: indexPath, cell :cell)
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return isEditing
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        delegate?.editCellAtIndexPath!(indexPath, editingStyle: editingStyle)
    }
}
