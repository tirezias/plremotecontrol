//
//  TableViewDataSource.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

@objc protocol TableViewDataSourceDelegate {
    
    @objc optional func currentCellRowNumber(indexPath: IndexPath, cell:UITableViewCell)
    @objc optional func lazyLoadWith(skip: Int, limit:Int)
    @objc optional func lazyLoad()
    @objc optional func editCellAtIndexPath(_ indexPath:IndexPath, editingStyle:UITableViewCell.EditingStyle)
}

class TableViewDataSource: NSObject, UITableViewDataSource, UITableViewDataSourcePrefetching {

    var arraySections:[[BaseViewModel]]! = []
    var items:[BaseViewModel]!
    var cellIdentifier: String = ""
    var isEditing: Bool = false
    var configureCellBlock: TableViewCellBlock = {_,_,_ in }
    weak var delegate:TableViewDataSourceDelegate?
    
    init(_ anItems: [BaseViewModel], aCellIdentifier: String, aConfigureCellBlock: @escaping TableViewCellBlock)  {
        
        super.init()
        
        self.items = anItems
        self.cellIdentifier = aCellIdentifier
        self.configureCellBlock = aConfigureCellBlock
        self.isEditing = false
    }
    
    func setItems(anItems: [BaseViewModel]) {
        
        items = anItems
    }
    
    func itemAtIndexPath(indexPath: IndexPath) -> BaseViewModel {
        
        var item: BaseViewModel = BaseViewModel()
        item.cellIdentifier = self.cellIdentifier
        if (indexPath.row < items.count) {
            
            item = items[indexPath.row]
        }
        
        return item
    }
    
    func removeItemAtIndex(indexPath: NSIndexPath) {
        
        if (indexPath.row < items.count) {
            
            items.remove(at: indexPath.row)
        }
    }
    
    func isLoadingCell(for indexPath: IndexPath) -> Bool {
        
        return indexPath.row >= items.count-1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(arraySections.count == 0) {
            
            return 1
        }
        
        return arraySections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item: BaseViewModel = itemAtIndexPath(indexPath: indexPath)
        if(!item.cellIdentifier.isEmpty) {
            
            cellIdentifier = item.cellIdentifier
        }
        
        let cell:BaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BaseTableViewCell
        configureCellBlock(cell, item, self.isEditing)
        if(delegate != nil) {
            
            delegate?.currentCellRowNumber!(indexPath: indexPath, cell :cell)
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return isEditing
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        delegate?.editCellAtIndexPath!(indexPath, editingStyle: editingStyle)
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        if (indexPaths.contains(where: isLoadingCell)) {
            
            delegate?.lazyLoad!()
        }
    }
}
