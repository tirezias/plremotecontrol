//
//  TableViewSectionDatasource.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

class TableViewSectionDatasource:NSObject, UITableViewDataSource, UITableViewDelegate {
 
    var arraySections:[[BaseViewModel]] = []
    var sectionTitles:[String] = []
    
    var cellIdentifier: String = ""
    var isEditing: Bool = false
    var configureCellBlock: TableViewCellBlock = {_,_,_ in }
    weak var delegate:TableViewDataSourceDelegate?
    
    init(_ anItems: [[BaseViewModel]], aCellIdentifier: String, aConfigureCellBlock: @escaping TableViewCellBlock)  {
        
        super.init()
        
        self.arraySections = anItems
        self.cellIdentifier = aCellIdentifier
        self.configureCellBlock = aConfigureCellBlock
        self.isEditing = false
    }

    func itemAtIndexPath(indexPath: IndexPath) -> BaseViewModel? {

        if(indexPath.section < arraySections.count) {
            
            let array:[BaseViewModel] = arraySections[indexPath.section]
            if(indexPath.row < array.count) {
                
                return array[indexPath.row]
            }
        }

        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return arraySections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arraySections[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let item: BaseViewModel = itemAtIndexPath(indexPath: indexPath) {
         
            let cell:BaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: item.cellIdentifier, for: indexPath) as! BaseTableViewCell
            configureCellBlock(cell, item, self.isEditing)
            if(delegate != nil) {
                
                delegate?.currentCellRowNumber!(indexPath: indexPath, cell :cell)
            }
            return cell
        }
        
        return tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return isEditing
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(sectionTitles.count > 0) {
            
            return sectionTitles[section]
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        delegate?.editCellAtIndexPath!(indexPath, editingStyle: editingStyle)
    }
    
//    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
//
//        if (indexPaths.contains(where: isLoadingCell)) {
//
//            delegate?.lazyLoad!()
//        }
//    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    var sectionTitles:[String]! = []
//    var sectionIndexTitles:[String]! = []
//    var arraySections:[[BaseViewModel]]! = []
    
//    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
//
//        return sectionIndexTitles
//    }
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//
//        return arraySections.count
//    }
//
//    override func isLoadingCell(for indexPath: IndexPath) -> Bool {
//
//        return indexPath.section >= arraySections.count-1
//    }
//
//    override func itemAtIndexPath(indexPath: IndexPath) -> BaseViewModel {
//
//        var item: BaseViewModel = BaseViewModel()
//        if (indexPath.section < arraySections.count) {
//
//            let sectionItems:[BaseViewModel] = arraySections[indexPath.section]
//            if(indexPath.row < sectionItems.count) {
//
//                item = sectionItems[indexPath.row]
//            }
//        }
//
//        return item
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        if (section < arraySections.count) {
//
//            return arraySections[section].count
//        }
//
//        return 0
//    }
//
//    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
//
//        if(sectionTitles.count > 0) {
//
//            return sectionTitles[section]
//        }
//        else {
//
//            return ""
//        }
//    }
//
//    override func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
//
//        if (indexPaths.contains(where: isLoadingCell)) {
//
//            delegate?.lazyLoad!()
//        }
//    }
}
