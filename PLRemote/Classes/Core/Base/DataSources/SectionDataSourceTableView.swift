//
//  SectionDataSourceTableView.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

@objc protocol SectionDataSourceTableViewDelegate:class {
    
    @objc optional func currentCellRowNumber(indexPath: IndexPath, cell:UITableViewCell)
}

class SectionDataSourceTableView: DataSourceTableView {

    var sectionTitles:NSArray = []
    var sectionIndexTitles:NSArray = []
    
    override func setItems(anItems: NSMutableArray) {
        
        items = anItems.mutableCopy() as! NSMutableArray
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        return sectionIndexTitles as? [String];
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return items.count
    }
    
    override func itemAtIndexPath(indexPath: IndexPath) -> BaseViewModel {
        
        var item: BaseViewModel = BaseViewModel()
        if (indexPath.section < items.count) {
            
            let sectionItems:NSArray = items.object(at: indexPath.section) as! NSArray
            if(indexPath.row < sectionItems.count) {
                
                item = sectionItems.object(at: indexPath.row) as! BaseViewModel
            }
        }
        
        return item
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (section < items.count) {
            
            let array:NSArray = items.object(at: section) as! NSArray
            
            return array.count
        }
        
        return 0
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {

        if(sectionTitles.count > 0) {
        
            return sectionTitles.object(at: section) as! String
        }
        else {
            
             return ""
        }
    }
}
