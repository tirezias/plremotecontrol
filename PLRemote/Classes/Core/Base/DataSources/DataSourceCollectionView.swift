//
//  DataSourceCollectionView.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

typealias CollectionViewCellBlock = (_ cell : BaseCollectionViewCell, _ id:AnyObject, _ isEditing: Bool) -> ()

class DataSourceCollectionView: NSObject, UICollectionViewDataSource {
    
    var items: NSMutableArray = []
    var cellIdentifier: String = ""
    var isEditing: Bool = false
    var configureCellBlock: CollectionViewCellBlock = {_,_,_ in }
    
    init(_ anItems: NSArray, aCellIdentifier: String, aConfigureCellBlock: @escaping CollectionViewCellBlock)  {
        
        super.init()
        
        self.items = anItems.mutableCopy() as! NSMutableArray
        self.cellIdentifier = aCellIdentifier
        self.configureCellBlock = aConfigureCellBlock
        self.isEditing = false
    }
    
    func setItems(anItems: NSArray) {
        
        items = anItems.mutableCopy() as! NSMutableArray
    }
    
    func itemAtIndexPath(indexPath: IndexPath) -> BaseViewModel {
        
        var item: BaseViewModel = BaseViewModel()
        if (indexPath.row < items.count) {
            
            item = items.object(at: indexPath.row) as! BaseViewModel
        }
        
        return item
    }
    
    func removeItem(item: AnyObject) {
        
        if (items.contains(item)) {
            
            items.remove(item)
        }
    }
    
    func removeItemAtIndex(indexPath: NSIndexPath) {
        
        if (indexPath.row < items.count) {
            
            items.removeObject(at: indexPath.row)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let item: BaseViewModel = self.itemAtIndexPath(indexPath: indexPath)
        
        cellIdentifier = item.cellIdentifier
        
        let cell: UICollectionViewCell  = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        
        cell.bounds = CGRect(x:0, y:0, width:collectionView.frame.size.width, height:cell.bounds.height)
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        self.configureCellBlock(cell as! BaseCollectionViewCell, item as AnyObject, self.isEditing)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
}
