//
//  BaseViewController.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var constraitForKeyboard:NSLayoutConstraint!
    
    var viewModel:BaseViewModel!

    
    // MARK: - Override methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupKeyboardConstraint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.navigateToSettings),
                                               name: NSNotification.Name(rawValue: OPEN_SETTINGS),
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: OPEN_SETTINGS), object: nil)
    }
    
    
    // MARK: - Public methods
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    @objc func navigateToSettings() {
        
        let storyboard = UIStoryboard.init(name: "Settings", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SettingsTableViewController")
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    // MARK: - Private methods
    private func setupKeyboardConstraint() {
        
        if(constraitForKeyboard != nil) {
         
            let window = UIApplication.shared.keyWindow
            var bottomPadding:CGFloat = 0
            if #available(iOS 11.0, *) {
                
                bottomPadding = (window?.safeAreaInsets.bottom)!
            }
            constraitForKeyboard.constant = bottomPadding
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        let userInfo = notification.userInfo!
        let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        if(self.constraitForKeyboard != nil) {
            
            UIView.animate(withDuration: 0.4) {
                
                self.constraitForKeyboard.constant = keyboardHeight
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        let window = UIApplication.shared.keyWindow
        var bottomPadding:CGFloat = 0
        if #available(iOS 11.0, *) {
            
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }

        let userInfo = notification.userInfo!
        let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        UIView.animate(withDuration: 0.4) {
            
            if(self.constraitForKeyboard != nil) {
                
                self.constraitForKeyboard.constant = keyboardHeight - bottomPadding
                self.view.layoutIfNeeded()
            }
        }
    }
}
