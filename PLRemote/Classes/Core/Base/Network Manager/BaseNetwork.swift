//
//  BaseNetwork.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

struct ErrorDataModel: Error {
    
    var message:String = ""
    var statusCode:ErrorStatus = ErrorStatus.success
}

class BaseNetwork: NSObject {

    let networkReachabilityManager:NetworkReachabilityManager = NetworkReachabilityManager()!
    
//    func header() -> [String : String] {
//
//        let token:String? = UserDefaults.standard.string(forKey: APP_TOKEN)
//        if(token != nil) {
//         
//            return ["Content-Type": "application/json",
//                    "Authorization": "Bearer " + token!]
//        }
//        else {
//            
//            return ["Content-Type": "application/json",
//                    "Authorization": "Bearer "]
//        }
//    }
    
//    func request(_ url: String!, method: HTTPMethod, parameters: Parameters?) -> DataRequest {
//        
//        if(!networkReachabilityManager.isReachable) {
//
//            let emptyURL: URLConvertible = "" as URLConvertible
//            return SessionManager.default.request(emptyURL)
//        }
//        else {
//
//            let strURL: String = BASE_URL + url
//            let urlString: URLConvertible = strURL as URLConvertible
//            if(method == .get) {
//
//                return SessionManager.default.request(urlString, method: method, parameters: parameters, encoding: URLEncoding.default, headers: header())
//            }
//            else {
//
//                return SessionManager.default.request(urlString, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: header())
//            }
//        }
//    }
    
//    func requestWithQueryParameters(_ url: String!, method: HTTPMethod, parameters: [String:Any]) -> DataRequest {
//
//        if(!networkReachabilityManager.isReachable) {
//
//            let emptyURL: URLConvertible = "" as URLConvertible
//            return SessionManager.default.request(emptyURL)
//        }
//        else {
//
//            let strURL:String = BASE_URL + url
//            let urlString: URLConvertible = strURL as URLConvertible
//            var url:URL = URL(string: "a")!
//            var components = URLComponents()
//            let queryItemToken = URLQueryItem(name: "token", value: "12345")
//            components.queryItems = [queryItemToken]
//            url.appendPathComponent(components.string!)
//
//            if(method == .get) {
//
//                return SessionManager.default.request(urlString, method: method, parameters: nil, encoding: URLEncoding.default, headers: header())
//            }
//            else {
//
//                return SessionManager.default.request(urlString, method: method, parameters: nil, encoding: JSONEncoding.default, headers: header())
//            }
//        }
//    }
}

//extension DataRequest {
//    
//    /// Adds a handler to be called once the request has finished.
//    ///
//    /// - parameter options:           The JSON serialization reading options. Defaults to `.allowFragments`.
//    /// - parameter completionHandler: A closure to be executed once the request has finished.
//    ///
//    /// - returns: The request.
//    @discardableResult
//    public func responseSwiftyJSON(queue: DispatchQueue? = nil,
//                                   options: JSONSerialization.ReadingOptions = .allowFragments,
//                                   completionHandler: @escaping (DataResponse<JSON>) -> Void) -> Self {
//        
//        return response(queue: queue,
//                        responseSerializer: DataRequest.swiftyJSONResponseSerializer(options: options),
//                        completionHandler: completionHandler)
//    }
//        
//    /// Creates a response serializer that returns a SwiftyJSON instance result type constructed from the response data using
//    /// `JSONSerialization` with the specified reading options.
//    ///
//    /// - parameter options: The JSON serialization reading options. Defaults to `.allowFragments`.
//    ///
//    /// - returns: A SwiftyJSON response serializer.
//    public static func swiftyJSONResponseSerializer(options: JSONSerialization.ReadingOptions = .allowFragments) -> DataResponseSerializer<JSON> {
//        
//        return DataResponseSerializer { _, response, data, error in
//        
//            let result = Request.serializeResponseJSON(options: options, response: response, data: data, error: error)
//            switch result {
//            case .success(let value):
//               
//                let (resultValue, errorDataModel) = DataRequest.handleResponse(response, value: value)
//                if(resultValue != nil) {
//
//                    return .success(JSON(resultValue!))
//                }
//                else {
//
//                    return .failure(errorDataModel!)
//                }
//            case .failure(let value):
//                
//                let (resultValue, errorDataModel) = DataRequest.handleResponse(response, value: value)
//                if(resultValue != nil) {
//                    
//                    return .success(JSON(resultValue!))
//                }
//                else {
//                    
//                    return .failure(errorDataModel!)
//                }
//            }
//        }
//    }
//    
//    static func handleResponse(_ response:HTTPURLResponse?, value:Any) -> (Any?, ErrorDataModel?)  {
//        
//        var errorDataModel:ErrorDataModel = ErrorDataModel()
//        if(response?.statusCode != 200 && response?.statusCode != 201) {
//            
//            if(response?.statusCode == 401) {
//                
//                // TODO - Unauthorized notification
//                return (nil, errorDataModel)
//            }
//            else {
//                
//                if let dict:NSDictionary = value as? NSDictionary {
//                    
//                    if let errorMessage:String = dict["error"] as? String {
//                        
//                        errorDataModel.message = errorMessage
//                    }
//                    return (nil, errorDataModel)
//                }
//                else {
//                    
//                    // TODO -
//                    return (nil, errorDataModel)
//                }
//            }
//        }
//        else {
//            
//            return (value, nil)
//        }
//    }
//}
class Connectivity {
    
    class func isConnectedToInternet() -> Bool {
        
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
