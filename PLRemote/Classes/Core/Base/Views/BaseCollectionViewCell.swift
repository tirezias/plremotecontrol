//
//  BaseCollectionViewCell.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {

    open func configureCell(item: BaseViewModel)  {
        
    }
}
