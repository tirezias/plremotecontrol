//
//  BaseDAO.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import SwiftyJSON

class BaseDAO: NSObject {
            
    func deleteModelListFromLocalDB(_ modelType:Object.Type) {
        
        let realm = try! Realm()
        let result = realm.objects(modelType)
        realm.beginWrite()
        realm.delete(result)
        do {
            
            try realm.commitWrite()
        }
        catch {
            
        }
    }
    
    func deleteModelFromLocalDB(_ modelType:Object.Type,id:Int){
        
        do {
            let realm = try Realm()
            let needToDelete = realm.objects(modelType).filter("id == %d",id)
            try realm.write {
             
                realm.delete(needToDelete)
            }
        }
        catch {
           
            print("there is error with delete Realm object ! : \(error)")
        }
    }
    
    func deleteDevices(){
       
        do {
            let realm = try Realm()
            let needToDelete = realm.objects(ModelDevice.self)
            try realm.write {
             
                realm.delete(needToDelete)
            }
        }
        catch {
            print("there is error with delete Realm object ! : \(error)")
        }
    }
    
    func addModelToLocalStorage(_ model:Object) {
        
        let realm = try! Realm()
        try! realm.write {
            
            realm.add(model, update: .all)
        }
    }
    
    func addModelsToLocalStorage(_ models:[Object]) {
        
        let realm = try! Realm()
        try! realm.write {
            
            realm.add(models)
        }
    }
    
    func getModelFromLocalStorage(_ modelType:Object.Type) -> Object? {
        
        let searchResults = try! Realm().objects(modelType)
        if(searchResults.count > 0) {
            
            return searchResults.first!
        }
        return nil
    }
    
    func getModelListFromLocalDB(_ modelType:Object.Type) -> [Object] {
        
        var result:[Object] = []
        let searchResults = try! Realm().objects(modelType)
        for obj in searchResults {
            
            result.append(obj)
        }
        return result
    }
    
    func parseDataToObject<T>(_ type: T.Type, from data: NSArray) throws -> [Codable] where T : Decodable {
        
        var array:[Codable] = []
        for obj in data {
            
            if let jsonData = try? JSONSerialization.data(withJSONObject: obj, options: .prettyPrinted) {
                
                if let model = try? JSONDecoder().decode(type.self, from: jsonData) {
                    
                    array.append(model as! Codable)
                }
            }
        }
        return array
    }
}

extension Realm {
    
    func writeAsync<T : ThreadConfined>(obj: T, errorHandler: @escaping ((_ error : Swift.Error) -> Void) = { _ in return }, block: @escaping ((Realm, T?) -> Void)) {
        
        let wrappedObj = ThreadSafeReference(to: obj)
        let config = self.configuration
        DispatchQueue(label: "background").async {
            autoreleasepool {
                
                do {
                    let realm = try Realm(configuration: config)
                    let obj = realm.resolve(wrappedObj)
                    try realm.write {
                        
                        block(realm, obj)
                    }
                }
                catch {
                    
                    errorHandler(error)
                }
            }
        }
    }
}
