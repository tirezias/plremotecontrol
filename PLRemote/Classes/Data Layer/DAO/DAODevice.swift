//
//  DAODevice.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 9/3/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import RealmSwift

class DAODevice: BaseDAO {

    func writeDeviceInLocalStorage(_ vmDevice:VMDevice) {
        
        let model:ModelDevice = ModelDevice().initWithViewModel(viewModel: vmDevice)
        model.lastConnectedDevice = true
        addModelToLocalStorage(model)
    }
    
    func getConnectedDevices() -> [ModelDevice] {
     
        let array:[ModelDevice] = getModelListFromLocalDB(ModelDevice.self) as! [ModelDevice]
        return array
    }
    
    func removeDeviceFromLocalStorage(identifier:String) {
        
        do {
            let realm = try Realm()
            let needToDelete = realm.objects(ModelDevice.self).filter("identifier == %d",identifier)
            try realm.write {
                realm.delete(needToDelete)
            }
        }catch {
            print("there is error with delete Realm object ! : \(error)")
        }
    }
    
    func getLastConnectedDevice() -> ModelDevice? {
        
        let realm = try! Realm()
        return realm.objects(ModelDevice.self).filter("lastConnectedDevice == true").first
    }
    
    func getDeviceWithIdentifier(_ identifier:String) -> ModelDevice? {
        
        let realm = try! Realm()
        return realm.objects(ModelDevice.self).filter("identifier == %@", identifier).first
    }
    
    func addNewGate(_ model:ModelGate) {
        
        if let lastModelId:Int = getLastGateId() {
            
            model.id = lastModelId + 1
        }
        else {
            
            model.id = 1
        }
       
        let nextNumber = UserDefaults.standard.integer(forKey: "nextNmber")
        UserDefaults.standard.set(nextNumber + 1, forKey: "nextNmber")
        addModelToLocalStorage(model)
    }
    
    func editGate(_ vmGate:VMGate) {
     
        let model:ModelGate = ModelGate().initWithViewModel(model: vmGate)
        addModelToLocalStorage(model)
    }
    
    func deleteGate(_ vmGate:VMGate) {
        
        deleteModelFromLocalDB(ModelGate.self,id : vmGate.id)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: PLAN_UPGRADE), object: nil, userInfo: nil)
        })
       
    }
    
    func getLastGateId() -> Int? {
        
        return UserDefaults.standard.integer(forKey: "nextNmber")
    }
    
    func getAvailableGateCount() -> Int {
        
        let tempIndex:Int? = UserDefaults.standard.integer(forKey: PURCHESED_INDEX)
        if (tempIndex != nil) {
        
            switch tempIndex {
            case 1:
                return 10
            case 2:
                return 25
            case 3:
                return 100000
            default:
                return 3
            }
        }
        return 3
    }
    
    func getGatesFromLocalStorage() -> [ModelGate] {
        
        let realm = try! Realm()
        let gates = realm.objects(ModelGate.self)
        return Array(gates)
    }
}
