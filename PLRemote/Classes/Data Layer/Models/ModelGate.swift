//
//  ModelGate.swift
//  PLRemote
//
//  Created by Ishkhan Gevorgyan on 9/3/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import RealmSwift

@objcMembers class ModelGate: Object {
    
    dynamic var id:Int = 0
    dynamic var data:Int16 = 0
    
    dynamic var MHz_300:Bool = true
    dynamic var MHz_310:Bool = false
    
    dynamic var name:String = ""
    dynamic var street:String = ""
    dynamic var city:String = ""
    dynamic var state:String = ""
    dynamic var zip:String = ""
    dynamic var latitude:Double = 0
    dynamic var longitude:Double = 0
    
    // Configuration keys
    dynamic var key1:Bool = false
    dynamic var key2:Bool = false
    dynamic var key3:Bool = false
    dynamic var key4:Bool = false
    dynamic var key5:Bool = false
    dynamic var key6:Bool = false
    dynamic var key7:Bool = false
    dynamic var key8:Bool = false
    dynamic var key9:Bool = false
    dynamic var key10:Bool = false
    
    override class func primaryKey() -> String {
        
        return "id"
    }
    
    func initWithViewModel(model:VMGate) -> ModelGate {
        
        id = model.id
        MHz_300 = model.MHz_300
        MHz_310 = model.MHz_310
        
        name = model.name
        if(model.location != nil) {
         
            street = model.location!.street
            city = model.location!.city
            state = model.location!.country
            zip = model.location!.zip
            latitude = model.location!.latitude
            longitude = model.location!.longitude
        }
        
        key1 = model.key1
        key2 = model.key2
        key3 = model.key3
        key4 = model.key4
        key5 = model.key5
        key6 = model.key6
        key7 = model.key7
        key8 = model.key8
        key9 = model.key9
        key10 = model.key10
        
        return self
    }
}
