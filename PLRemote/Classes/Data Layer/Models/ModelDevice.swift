//
//  ModelDevice.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 9/3/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import RealmSwift

@objcMembers class ModelDevice: Object {

    dynamic var identifier:String = ""
    dynamic var deviceName:String = ""
    dynamic var lastConnectedDevice:Bool = false
    
    override class func primaryKey() -> String {
        
        return "identifier"
    }
    
    func initWithViewModel(viewModel:VMDevice) -> ModelDevice {
        
        identifier = viewModel.identifier
        deviceName = viewModel.deviceName
        
        return self
    }
}
