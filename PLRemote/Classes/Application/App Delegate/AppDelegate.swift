//
//  AppDelegate.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        customizeNavigationBar()
        setupRealm()
        IAPHandler.receiptValidation()
        FirebaseApp.configure()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
     
        _ = BLE.shared.endTransmit()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        LocationManager.sharedInstance.start()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    
    // MARK: - ==========================================================
    // MARK: - Setup Realm
    private func setupRealm() {
        
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        Realm.Configuration.defaultConfiguration = config
        if(Realm.Configuration.defaultConfiguration.fileURL != nil) {
            
            print("REALM FILE URL --------------- ", Realm.Configuration.defaultConfiguration.fileURL!)
        }
    }
    
    private func customizeNavigationBar() {
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor(hexString: "1C1C1C")
        navigationBarAppearace.barTintColor = .white
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor(hexString: "1C1C1C"),
                                                      NSAttributedString.Key.font: UIFont(name: "Nunito-Bold", size: 17)!] as [NSAttributedString.Key : Any]
        
        // Set back button indicator image
        var backButtonBackgroundImage = UIImage(named: "imageTransparent")!
        backButtonBackgroundImage = backButtonBackgroundImage.resizableImage(withCapInsets: UIEdgeInsets(top: 0,
                                                                                                         left:backButtonBackgroundImage.size.width - 1,
                                                                                                         bottom: 0,
                                                                                                         right: 0))
        navigationBarAppearace.backIndicatorImage = backButtonBackgroundImage
        navigationBarAppearace.backIndicatorTransitionMaskImage = backButtonBackgroundImage
        
        // Set back button background image
        let backButtonImage = UIImage(named: "iconNavigationBack")!
        UIBarButtonItem.appearance().setBackButtonBackgroundImage(backButtonImage, for: .normal, barMetrics: UIBarMetrics.default)
    }
}

