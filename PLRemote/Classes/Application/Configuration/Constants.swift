//
//  Constants.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 8/27/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//


import UIKit
import RealmSwift
import ObjectMapper

// MARK: - Constants
let FIRMWARE_VERSION: String = "1.1.0"
let verifyReceiptURL:String = "https://sandbox.itunes.apple.com/verifyReceipt"
let verifyReceiptURLLive:String = "https://buy.itunes.apple.com/verifyReceipt"

// In APP Purchase
let iTunesKey = "85b301993d4842598dc0c1774efe424f"


let HELP_ALREADY_OPENED:String = "helpAlreadyOpened"
let PURCHESED_INDEX:String = "PURCHESED_INDEX"


// Notifications
let DEVICE_FOUND: String = "deviceFound"
let PLAN_UPGRADE: String = "planupgrade"
let OPEN_SETTINGS: String = "openSettings"

let BLUETOOTH_STATE: String = "bluetoothState"
let CONNECTION_STATE: String = "connectionState"

// Texts
let ALERT_NEED_HELP_MESSAGE: String = "1. Replace the key for battery.\n2. Make sure PL Remote is in close proximity to your phone\n3. Make sure Bluetooth is turned on your phone."
let ALERT_NEED_HELP_TITLE: String = "Need Help?"

let ALERT_NEED_CONNECTION_MESSAGE: String = "Please connect to internet to have ability to choose a point on map"
let ALERT_NEED_CONNECTION_TITLE: String = "No internet connection"

let ALERT_UPDATE_PlAN_MESSAGE: String = "You are already using all your controllers. For More Users you need to upgrade your plan."
let ALERT_UPDATE_PlAN_TITLE: String = "Please upgrade your plan"
let DEVICE_NAME = "Gate"

// MARK: - Error texts
let REQUIRED_FIELDS = "Required fields"

// MARK: - Enums
enum ErrorStatus: Int {
    
    case success
    
    case fail
    
    case noInternet
}

enum Device: Int {
    
    case DIP_10
    
    case DIP_8
}

let SCAN_TIME               = 4.0
let SPLSH_SHOWING_TIME      = 1.5
let MYLE                    = 1608.0
let FTMULTYPLAYER           = 3.28084
let MAP_EDGE_INSETS         = UIEdgeInsets(top: 60, left: 0, bottom: 60, right: 0)
