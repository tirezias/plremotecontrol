//
//  VMLocationSearch.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 7/21/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit
import MapKit

class VMLocationSearch: BaseViewModel {

    var placeMark:MKPlacemark!
    
    init(placemark:MKPlacemark) {
        
        super.init()
        
        self.placeMark = placemark
        self.cellIdentifier = CellIdentifierLocation
    }
}
