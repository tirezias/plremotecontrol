//
//  VMDevice.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 9/4/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreBluetooth

class VMDevice: BaseViewModel {

    var peripheral:CBPeripheral?
    var deviceName:String = ""
    var identifier:String = ""
    var isConnected:Bool = false
    var lastConnectedDevice:Bool = false
    var isFirstDevice:Bool = false
    var isSelected:Bool = false
    
    init(name:String, identifier:String, peripheral:CBPeripheral) {
        
        super.init()
        
        self.peripheral = peripheral
        self.deviceName = name
        self.identifier = identifier
        self.cellIdentifier = CellIdentifierNearDevices
    }
    
    init(model:ModelDevice) {
        
        super.init()
        
        identifier = model.identifier
        deviceName = model.deviceName
    }
    
    override init() {
        
    }
}
