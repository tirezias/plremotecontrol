//
//  VMGate.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 9/4/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import MapKit

class VMGate: BaseViewModel, MKAnnotation {

    var id:Int = 0
    var name:String = ""
    var data:Int16 = 0
    
    var MHz_300:Bool = false
    var MHz_310:Bool = false
    var distance:Double = 0.0
    
    // Configuration keys
    var key1:Bool = false
    var key2:Bool = false
    var key3:Bool = false
    var key4:Bool = false
    var key5:Bool = false
    var key6:Bool = false
    var key7:Bool = false
    var key8:Bool = false
    var key9:Bool = false
    var key10:Bool = false
    
    var isTransmited:Bool = false
    var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(0, 0)
    var location:VMLocation?
    
    func initWithViewModel(model:ModelGate) -> VMGate {
        
        id = model.id
        data = model.data
        
        MHz_300 = model.MHz_300
        MHz_310 = model.MHz_310
        name = model.name
        location = VMLocation(latitude: model.latitude,
                              longitude: model.longitude,
                              street: model.street,
                              city: model.city,
                              country: model.state,
                              zip: model.zip)
        coordinate = CLLocationCoordinate2D(latitude: model.latitude, longitude: model.longitude)
        
        key1 = model.key1
        key2 = model.key2
        key3 = model.key3
        key4 = model.key4
        key5 = model.key5
        key6 = model.key6
        key7 = model.key7
        key8 = model.key8
        key9 = model.key9
        key10 = model.key10
        
        calculateData()
        
        return self
    }
    
    func calculateData() {
        
        data = 0
        if(key1) {
            
            data += 1
        }
        if(key2) {
            
            data += 2
        }
        if(key3) {
            
            data += 4
        }
        if(key4) {
            
            data += 8
        }
        if(key5) {
            
            data += 16
        }
        if(key6) {
            
            data += 32
        }
        if(key7) {
            
            data += 64
        }
        if(key8) {
            
            data += 128
        }
        if (MHz_310 == true) {
            
            if(key9) {
                
                data += 256
            }
            if(key10) {
                
                data += 512
            }
        }
    }
}
