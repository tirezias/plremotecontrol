//
//  VMAddNewDevice.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 7/14/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class VMAddNewDevice: VMDevice {

    var isExistDevice:Bool = false
    
    init(isExistDevice:Bool) {
        
        super.init()
        
        self.isExistDevice = isExistDevice
        self.cellIdentifier = CellIdentifierAddDevice
    }
}
