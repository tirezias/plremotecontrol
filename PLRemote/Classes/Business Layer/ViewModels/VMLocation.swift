//
//  VMLocation.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 7/3/20.
//  Copyright © 2020 Karen Petrosyan. All rights reserved.
//

import UIKit

class VMLocation: BaseViewModel {

    var street:String = ""
    var city:String = ""
    var country:String = ""
    var zip:String = ""
    var addressFull:String = ""
    
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    
    override init() {
        
    }
    
    init(latitude:Double, longitude:Double, street:String?, city:String?, country:String?, zip:String?) {
        
        self.latitude = latitude
        self.longitude = longitude
        if (street != nil) {
            
            self.street = street!
            addressFull = addressFull + street! + ", "
        }
        if (city != nil) {
         
            self.city = city!
            addressFull = addressFull + city! + ", "
        }
        if (country != nil) {
         
            self.country = country!
            addressFull = addressFull + country! + ", "
        }
        if(zip != nil) {
            
            self.zip = zip!
        }
    }
}
