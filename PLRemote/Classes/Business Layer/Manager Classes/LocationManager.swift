//
//  LocationManager.swift
//  PLRemote
//
//  Created by Ishkhan Gevorgyan on 9/3/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit
import CoreLocation

let HeadingFilter:Double = 10
let DefaultTimeInterval:Double = 3
let LocationTimeIntervalMLS:Double = 100

public typealias LocationHandlerPaused = ((CLLocation?) -> Void)
public typealias HeadingHandlerPaused = ((Double?) -> Void)
var noLocationView = UIView()
var noLocationViewIsHidden = true

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    fileprivate var latestHeading:Double?
    var callback:LocationHandlerPaused?
    var headingCallbacks = [HeadingHandlerPaused?]()
    var forOneShot = false
    var country = ""
    
    var latestLocation:CLLocation? {
        
        didSet {
            
            if (forOneShot) {
                
                forOneShot = false
                self.stop()
            }
        }
    }
    var manager:CLLocationManager?
    
    static let sharedInstance = LocationManager()
    override init() {
        
    }
    
    func start() {
        
        forOneShot = false
        if (manager == nil) {
            
            self.manager = CLLocationManager()
            self.manager!.delegate = self
            self.manager!.headingFilter = HeadingFilter
            self.manager!.desiredAccuracy = kCLLocationAccuracyBest
            self.manager!.distanceFilter = 5
        }
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            if (CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways) {
                
                noLocationView.isHidden = true
                self.startUpdating()
            }
            else if CLLocationManager.authorizationStatus() == .denied {
                
               // self.requestLocationServices()
            }
            else {
                
                self.manager?.requestWhenInUseAuthorization()
            }
        } else {
            
           // self.requestLocationServices()
        }
        
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.perform(#selector(LocationManager.checkIfNoLocation), with: nil, afterDelay: DefaultTimeInterval)
    }
    
    func stop() {
        
        self.manager?.stopUpdatingLocation()
        self.manager?.stopUpdatingHeading()
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        
    }
    
    func startForOneShot() {
        
        start()
        forOneShot = true
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last!
        latestLocation = location
        if (self.latestLocation != nil) {
            
            let mill1 = (location.timestamp.timeIntervalSince1970)*1000
            let mill2 = (self.latestLocation?.timestamp.timeIntervalSince1970)!*1000
            if (mill1-mill2 > LocationTimeIntervalMLS) {
                
                if (latestHeading != nil) {
                    
                    handleHeading((latestHeading)!)
                }
                self.handleLocation(location)
            }
        }
        else {
            
            self.handleLocation(location)
        }
    }
        
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        handleHeading(newHeading.trueHeading)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            
            self.startUpdating()
        }
    }
        
    func handleLocation(_ foundLocation:CLLocation?) {
        
//        self.latestLocation = foundLocation
        if (callback != nil) {
            
            callback!(foundLocation)
        }
    }
    
    func checkStatus() {
        
        if (CLLocationManager.locationServicesEnabled()) {
            
            if (CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways) {
                
                noLocationViewIsHidden = true
            }
            else if (CLLocationManager.authorizationStatus() == .denied) {
                
               // self.requestLocationServices()
            }
            else {
                
                self.manager?.requestWhenInUseAuthorization()
            }
        }
        else {
           // self.requestLocationServices()
        }
    }
    
    func startUpdating() {
        
        self.manager?.startUpdatingLocation()
        self.manager?.startUpdatingHeading()
    }
    
    func handleHeading(_ bearing:Double) {
        
        self.latestHeading = bearing
        for item in headingCallbacks {
            
            if (item != nil) {
                
                item!(bearing)
            }
        }
    }
    
    func registerForHeading(callback: HeadingHandlerPaused?) {
        
        if (latestHeading != nil) {
            
            callback!(latestHeading)
        }
        self.headingCallbacks.append(callback)
    }
    
    func registerForLocation(callback:@escaping LocationHandlerPaused) {
        
        self.callback = callback
    }
    
    @objc
    func checkIfNoLocation() {
        
        if (callback != nil) {
            
            callback!(nil)
        }
    }
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
}

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        
        if let index = firstIndex(of: object) {
            
            remove(at: index)
        }
    }
}

