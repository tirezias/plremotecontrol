//
//  BluetoothManager.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 9/2/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import Foundation
import CoreBluetooth

class BluetoothManager:NSObject, BLEDelegate {
    
    static let shared = BluetoothManager()
    
    var ble:BLE!
    let serviceDevice = ServiceDevice()
    var vmDevice:VMDevice?
    var isConnected:Bool {
        
        if(vmDevice != nil) {
            
            return vmDevice!.isConnected
        }
        return false
    }
    
    func setupBLE() {
        
        if(ble == nil) {

            ble = BLE()
        }
        if(ble.mDelegate == nil) {

            ble.mDelegate = self
        }
        if(ble.mCentralManager == nil) {

            ble.startCentralManager()
        }
    }
    
    func startScan() {
        
        ble.startScan()
    }
    
    func stopScan() {
        
        ble.stopScan()
        _ = ble.endTransmit()
    }
    
    func connect(_ device:VMDevice) {
        
        if(device.peripheral != nil) {
                
            ble.connect(peripheral: device.peripheral!)
        }
    }
    
    func disconnect() {
        
        ble.disconnect()
        vmDevice = nil
    }
    
    func changeDeviceConnectedStatus(device:VMDevice, isConnected:Bool) {
        
        if(isConnected) {
            
            if(self.vmDevice == nil) {
                
                self.vmDevice = device
            }
            self.vmDevice!.isConnected = isConnected
        }
        else {
            
            self.vmDevice = nil
        }
    }
    
    func getConnectedDeviceIdentifier() -> String {
        
        if(vmDevice != nil) {
            
            return vmDevice!.identifier
        }
        
        return ""
    }
    
    func getConnectedDeviceStatus() -> Bool {
        
        if(vmDevice != nil) {
            
            return vmDevice!.isConnected
        }
        
        return false
    }
    

    // MARK: - BLEDelegate methods
    func OnScanResult(_ importer: BLE, peripheral: CBPeripheral, deviceName: String) {
        
        print("======== OnScanResult")
        print("======== peripheral", peripheral)

        if(deviceName.contains(DEVICE_NAME)) {
         
            let viewModel:VMDevice = VMDevice(name: deviceName, identifier: peripheral.identifier.uuidString, peripheral: peripheral)
            let info = [DEVICE_FOUND : viewModel]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: DEVICE_FOUND), object: nil, userInfo: info)
        }
    }
    
    func OnConnectionState(_ importer: BLE, state: Bool) {
        
        print("======== OnConnectionState")
        print("======== state", state)
        if(vmDevice != nil) {
            
            vmDevice!.isConnected = state
        }
        if(state) {
            
            serviceDevice.writeDeviceInLocalStorage(vmDevice!)
        }
        let info = [CONNECTION_STATE : state]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CONNECTION_STATE), object: nil, userInfo: info)
    }
    
    func OnBluetoothState(_ importer: BLE, state: Bool) {
        
        print("======== OnBluetoothState")
        print("======== state", state)
        ble.startScan()
    }
    
    func OnButtonPressed(_ importer: BLE) {
        
        print("======== OnButtonPressed")
        print("======== importer", importer)
    }
}
