//
//  ServiceDevice.swift
//  PLRemote
//
//  Created by Karen Petrosyan on 9/3/19.
//  Copyright © 2019 Karen Petrosyan. All rights reserved.
//

import UIKit

class ServiceDevice: BaseService {

    let daoDevice = DAODevice()
    
    func writeDeviceInLocalStorage(_ vmDevice:VMDevice) {
        
        daoDevice.writeDeviceInLocalStorage(vmDevice)
    }
    
    func changeDeviceConnectedStatus(device:VMDevice, isConnected:Bool) {
     
        BluetoothManager.shared.changeDeviceConnectedStatus(device: device, isConnected: isConnected)
    }
    
    func getConnectedDevices() -> [VMDevice] {
     
        var result:[VMDevice] = []
        var index:Int = 0
        let connectedDeviceIdentifier:String = BluetoothManager.shared.getConnectedDeviceIdentifier()
        let connectedDeviceStatus:Bool = BluetoothManager.shared.getConnectedDeviceStatus()
        for obj in daoDevice.getConnectedDevices() {
            
            let vm:VMDevice = VMDevice(model: obj)
            if(connectedDeviceIdentifier == vm.identifier) {
                
                vm.isConnected = connectedDeviceStatus
            }
            vm.isFirstDevice = index == 0 ? true : false
            index += 1
            vm.cellIdentifier = CellIdentifierMyDevice
            result.append(vm)
        }
        
        let vmAddNewDevice = VMAddNewDevice(isExistDevice: result.count > 0)
        result.append(vmAddNewDevice)
        
        return result
    }
    
    func removeDeviceFromLocalStorage(identifier:String) {
     
        daoDevice.removeDeviceFromLocalStorage(identifier: identifier)
    }
    
    func getLastConnectedDevice() -> VMDevice? {
        
        if let model:ModelDevice = daoDevice.getLastConnectedDevice() {
            
            return VMDevice(model: model)
        }
        
        return nil
    }
    
    func getDeviceWithIdentifier(_ identifier:String) -> VMDevice? {
        
        if let model:ModelDevice = daoDevice.getDeviceWithIdentifier(identifier) {
            
            return VMDevice(model: model)
        }
        
        return nil
    }
    
    func addNewGate(_ vmGate:VMGate) {
        
        let model:ModelGate = ModelGate().initWithViewModel(model: vmGate)
        daoDevice.addNewGate(model)
    }
    
    func editGate(_ vmGate:VMGate) {
        
        daoDevice.editGate(vmGate)
    }
    
    func deleteGate(_ vmGate:VMGate) {
        
        daoDevice.deleteGate(vmGate)
    }
    
    func getGatesFromLocalStorage() -> [VMGate] {
        
        var result:[VMGate] = []
        let gates = daoDevice.getGatesFromLocalStorage()
        for model in gates {
            
            let vmGate:VMGate = VMGate().initWithViewModel(model: model)
            if (vmGate.id != 0) {
                
                result.append(vmGate)
            }
        }
        
        return result
    }
    
    func getGatesCount() -> Int {

        return daoDevice.getGatesFromLocalStorage().count
    }
    
    func getAvailableGateCount() -> Int {

        return daoDevice.getAvailableGateCount()
    }
    
    func getLastGateId() -> Int {
     
        if let id:Int = daoDevice.getLastGateId() {
            
            return id
        }
        return 0
    }
}
